# Stirrups

GTK4 Demos with alternative languages.

![A stirrups screenshot](data/screenshots/screenshot1.png)

## Sources

The application is a port of [org.gtk.Demo4](https://gitlab.gnome.org/GNOME/gtk/tree/master/demos/gtk-demo) from C to JavaScript.
Demos are currently written in JavaScript and Python. Others can be easily added.

## Purpose

The purpose is twofold:

1. Provide to the user many examples of widget use in an accessible language.
2. Provide to the contributor the opportunity to practice GTK4.

## Dependencies

The syntax hihjlighting is optionally provided by the [highlight](https://www.andre-simon.de/doku/highlight/highlight.html) program.

## Run

### Builder

The simplest way is to run it in [GNOME Builder](https://wiki.gnome.org/Apps/Builder).

### Flatpak

```sh
flatpak-builder --force-clean --system build-dir org.codeberg.som.Stirrups.json
flatpak-builder --run build-dir org.codeberg.som.Stirrups.json org.codeberg.som.Stirrups
```

## Install

### Host

```sh
meson build && cd build && ninja && sudo ninja install
```

### Flatpak

```sh
flatpak-builder --install --system --force-clean build-dir org.codeberg.som.Stirrups.json
flatpak run org.codeberg.som.Stirrups
```

## Uninstall

### Host

```sh
cd build && sudo ninja uninstall
```

### Flatpak

```sh
flatpak remove --delete-data org.codeberg.som.Stirrups
```
