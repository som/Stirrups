desktop_file = i18n.merge_file(
  input: configure_file(
    input: '@0@.desktop.in'.format('app_id'),
    output: '@0@.desktop'.format('app_id'),
    configuration: metadata
  ),
  output: '@0@.desktop'.format(app_id),
  type: 'desktop',
  po_dir: '../po',
  install: true,
  install_dir: datadir / 'applications'
)

desktop_utils = find_program('desktop-file-validate', required: false)
if desktop_utils.found()
  test('Validate desktop file', desktop_utils,
    args: [desktop_file]
  )
endif

configure_file(
  input: 'app_id.gschema.xml.in',
  output: '@0@.gschema.xml'.format(app_id),
  configuration: metadata,
  install: true,
  install_dir: datadir / 'glib-2.0/schemas'
)

compile_schemas = find_program('glib-compile-schemas', required: false)
if compile_schemas.found()
  test('Validate schema file', compile_schemas,
    args: ['--strict', '--dry-run', meson.current_source_dir()]
  )
endif

appstream_file = i18n.merge_file(
  input: configure_file(
    input: '@0@.metainfo.xml.in'.format('app_id'),
    output: '@0@.metainfo.xml'.format('app_id'),
    configuration: metadata
  ),
  output: '@0@.metainfo.xml'.format(app_id),
  po_dir: '../po',
  install: true,
  install_dir: datadir / 'metainfo'
)

appstream_util = find_program('appstream-util', required: false)
if appstream_util.found()
  test('Validate appstream file', appstream_util,
    args: ['validate', appstream_file]
  )
endif

install_data(
    'icons/@0@.svg'.format('app_id'),
    install_dir: datadir / 'icons' / 'hicolor' / 'scalable' / 'apps',
    rename: '@0@.svg'.format(app_id)
)

install_data(
    'icons/@0@-symbolic.svg'.format('app_id'),
    install_dir: datadir / 'icons' / 'hicolor' / 'symbolic' / 'apps',
    rename: '@0@-symbolic.svg'.format(app_id)
)
