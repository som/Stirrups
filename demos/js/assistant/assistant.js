// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Assistant
 *
 * Demonstrates a sample multi-step assistant with GtkAssistant. Assistants
 * are used to divide an operation into several simpler sequential steps,
 * and to guide the user through these steps.
 */

imports.gi.versions['Gtk'] = '4.0';
const { GLib, Gtk } = imports.gi;

const BOX_PARAMS = {
    marginStart: 12, marginEnd: 12, marginTop: 12, marginBottom: 12, spacing: 12,
    halign: Gtk.Align.CENTER, valign: Gtk.Align.CENTER
};

function onAssistantApply(assistant) {
    GLib.timeout_add(GLib.PRIORIRY_DEFAULT, 100, () => {
        let progressBar = assistant.get_nth_page(3).get_first_child();
        let fraction = progressBar.fraction;
        fraction += 0.05;

        if (fraction < 1.0) {
            progressBar.fraction = fraction;
            return GLib.SOURCE_CONTINUE;
        } else {
            assistant.destroy();
            return GLib.SOURCE_REMOVE;
        }
    });
}

function onAssistantCloseCancel(assistant) {
    assistant.destroy();
}

function onAssistantPrepare(assistant, page) {
    // The fourth page (counting from zero) is the progress page. The user clicked Apply
    // to get here so we tell the assistant to commit, which means the changes up to this
    // point are permanent and cannot be cancelled or revisited.
    if (assistant.get_current_page() == 3)
        assistant.commit();
}

function createPage1(assistant) {
    let box = new Gtk.Box(BOX_PARAMS);

    let label = new Gtk.Label({ label: "You must fill out this entry to continue:" });
    box.append(label);

    let entry = new Gtk.Entry({ activatesDefault: true, valign: Gtk.Align.CENTER });
    entry.connect('changed', () => {
        assistant.set_page_complete(box, Boolean(entry.text));
    });
    box.append(entry);

    // TODO: report Accessible update_relation issue
    //entry.update_relation([Gtk.AccessibleRelation.LABELLED_BY], [label]);

    assistant.append_page(box);
    assistant.set_page_title(box, "Page 1");
    assistant.set_page_type(box, Gtk.AssistantPageType.INTRO);
}

function createPage2(assistant) {
    let box = new Gtk.Box(BOX_PARAMS);

    let checkButton = new Gtk.CheckButton({ label: "This is optional data, you may continue even if you do not check this" });
    box.append(checkButton);

    assistant.append_page(box);
    assistant.set_page_title(box, "Page 2");
    assistant.set_page_complete(box, true);
}

function createPage3(assistant) {
    let box = new Gtk.Box({ ...BOX_PARAMS });

    let label = new Gtk.Label({ label: "This is a confirmation page, press 'Apply' to apply changes" });
    box.append(label);

    assistant.append_page(box);
    assistant.set_page_title(box, "Confirmation");
    assistant.set_page_type(box, Gtk.AssistantPageType.CONFIRM);
    assistant.set_page_complete(box, true);
}

function createPage4(assistant) {
    let box = new Gtk.Box({ ...BOX_PARAMS });

    let progressBar = new Gtk.ProgressBar({ hexpand: true, marginStart: 40, marginEnd: 40, valign: Gtk.Align.CENTER });
    box.append(progressBar);

    assistant.append_page(box);
    assistant.set_page_title(box, "Applying changes");
    assistant.set_page_type(box, Gtk.AssistantPageType.PROGRESS);

    // This prevents the assistant window from being closed while we're "busy" applying changes.
    assistant.set_page_complete(box, false);
}

let application = new Gtk.Application();

application.connect('activate', () => {
    let assistant = new Gtk.Assistant({ application, defaultHeight: 300 });

    createPage1(assistant);
    createPage2(assistant);
    createPage3(assistant);
    createPage4(assistant);

    assistant.connect('cancel', onAssistantCloseCancel);
    assistant.connect('close', onAssistantCloseCancel);
    assistant.connect('apply', onAssistantApply);
    assistant.connect('prepare', onAssistantPrepare);

    assistant.show();
});

application.run([]);
