// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Clipboard
 *
 * GdkClipboard is used for clipboard handling. This demo shows how to
 * copy and paste text to and from the clipboard.
 *
 * It also shows how to transfer images via the clipboard or via
 * drag-and-drop, and how to make clipboard contents persist after
 * the application exits. Clipboard persistence requires a clipboard
 * manager to run.
 *
 * In addition to the original GTK demo, images can be pasted as icon
 * name text, and vice-versa.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const {  Gtk } = imports.gi;
const { DemoEntry, DemoImage } = imports.demowidgets;
const _ = imports.gettext.domain('gtk40').gettext;

Gtk.Box.prototype.with = function(...widgets) {
    widgets.forEach(widget => this.append(widget));
    return this;
};

let application = new Gtk.Application();

application.connect('activate', () => {
    let copyButton, pasteButton, hboxParams = {
        orientation: Gtk.Orientation.HORIZONTAL, spacing: 4,
        marginStart: 8, marginEnd: 8, marginTop: 8, marginBottom: 8,
    };

    new Gtk.Window({
        application,
        title: "Clipboard",
        child: new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            marginStart: 8, marginEnd: 8,
            marginTop: 8, marginBottom: 8,
        }).with(
            Gtk.Label.new("\"Copy\" will copy the text\nin the entry to the clipboard"),
            new Gtk.Box(hboxParams).with(
                new DemoEntry(),
                copyButton = Gtk.Button.new_with_mnemonic(_("_Copy")),
            ),
            Gtk.Label.new("\"Paste\" will paste the text from the clipboard to the entry"),
            new Gtk.Box(hboxParams).with(
                new DemoEntry(),
                pasteButton = Gtk.Button.new_with_mnemonic(_("_Paste")),
            ),
            Gtk.Label.new("Images can be transferred via the clipboard, too"),
            new Gtk.Box(hboxParams).with(
                new DemoImage({ iconName: 'dialog-warning' }),
                new DemoImage({ iconName: 'process-stop' }),
                new DemoImage({ iconName: 'weather-clear' }),
            ),
        ),
    }).present();

    copyButton.connect('clicked', button => {
        let entry = button.get_prev_sibling();
        entry.copy();
    });

    pasteButton.connect('clicked', button => {
        let entry = button.get_prev_sibling();
        entry.pasteAsync().catch(error => {
            // Show an error about why pasting failed.
            // Usually you probably want to ignore such failures,
            // but for demonstration purposes, we show the error.
            new Gtk.MessageDialog({
                transientFor: button.root, destroyWithParent: true, modal: true,
                messageType: Gtk.MessageType.ERROR,
                buttons: Gtk.ButtonsType.CLOSE,
                text: `Could not paste text: ${error.message}`,
                visible: true,
            }).connect('response', dialog => dialog.destroy());
        });
    });
});

application.run([]);
