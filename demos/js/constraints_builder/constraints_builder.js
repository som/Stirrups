// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Builder
 *
 * GtkConstraintLayouts can be created in .ui files, and constraints can
 * be set up at that time as well, as this example demonstrates. It shows
 * various ways to do spacing and sizing with constraints.
 *
 * Make the window wider to see the rows react differently
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GObject, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');

// Register the widget class to instantiate in the builder file.
// TODO: Report issue to GJS: Init params contains 3 keys for the same 'css-name' property.
// GLib-GObject-CRITICAL **: g_object_new_is_valid_property: property 'css-name' for type 'ConstraintsGrid' cannot be set twice
GObject.registerClass({
    GTypeName: 'ConstraintsGrid',
}, class extends Gtk.Widget {
    // Cannot use vfunc_dispose() in GJS because it conflicts with the garbage collector.
    vfunc_unrealize() {
        super.vfunc_unrealize();

        let child;
        while ((child = this.get_last_child()))
            child.unparent();
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let builder = Gtk.Builder.new_from_file(directory.get_child('constraints_builder.ui').get_path());
    let window = builder.get_object('window1');

    application.add_window(window);
    window.present();
});

application.run([]);
