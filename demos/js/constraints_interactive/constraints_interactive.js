// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Interactive Constraints
 *
 * This example shows how constraints can be updated during user interaction.
 * The vertical edge between the buttons can be dragged with the mouse.
 *
 * FIXME: Buggy such as in GTK4 Demo.
 */

imports.gi.versions['Gtk'] = '4.0';
const { GObject, Gtk } = imports.gi;

const InteractiveGrid = GObject.registerClass(class InteractiveGrid extends Gtk.Widget {
    _init(widgets, params) {
        super._init(params);

        this._guide = new Gtk.ConstraintGuide();
        this.layoutManager.add_guide(this._guide);
        this._addConstraintGuide();

        widgets.forEach((widget, index) => {
            widget.set_parent(this);

            if (index % 2)
                this._addConstraintsOdd();
            else
                this._addConstraintsEven();

            if (index == 0)
                this._addConstraintFirst();
            else
                this._addConstraintSibling();

            if (index == widgets.length - 1)
                this._addConstraintLast();
        });

        let dragGesture = new Gtk.GestureDrag();
        dragGesture.connect('drag-update', this._onDragUpdated.bind(this));
        this.add_controller(dragGesture);
    }

    // Cannot use vfunc_dispose() in GJS because it conflicts with the garbage collector.
    vfunc_unrealize() {
        super.vfunc_unrealize();

        let child;
        while ((child = this.get_last_child()))
            child.unparent();
    }

    _onDragUpdated(dragGesture, offsetX, offsetY) {
        if (this._constraint)
            this.layoutManager.remove_constraint(this._constraint);

        let [success_, x, y_] = dragGesture.get_start_point();
        this._constraint = new Gtk.Constraint({
            target: this._guide,
            targetAttribute: Gtk.ConstraintAttribute.LEFT,
            relation: Gtk.ConstraintRelation.EQ,
            constant: x + offsetX,
            strength: Gtk.ConstraintStrength.REQUIRED,
        });
        this.layoutManager.add_constraint(this._constraint);

        this.queue_allocate();
    }

    _addConstraintGuide() {
        // guide.width = 0
        this.layoutManager.add_constraint(
            new Gtk.Constraint({
                target: this._guide,
                targetAttribute: Gtk.ConstraintAttribute.WIDTH,
                relation: Gtk.ConstraintRelation.EQ,
                constant: 0,
                strength: Gtk.ConstraintStrength.REQUIRED,
            })
        );
    }

    _addConstraintsOdd() {
        // child.start = guide.end
        this.layoutManager.add_constraint(
            new Gtk.Constraint({
                target: this.get_last_child(),
                targetAttribute: Gtk.ConstraintAttribute.START,
                relation: Gtk.ConstraintRelation.EQ,
                source: this._guide,
                sourceAttribute: Gtk.ConstraintAttribute.END,
                multiplier: 1,
                constant: 0,
                strength: Gtk.ConstraintStrength.REQUIRED,
            })
        );

        // child.end = super.end - 8
        this.layoutManager.add_constraint(
            new Gtk.Constraint({
                target: this.get_last_child(),
                targetAttribute: Gtk.ConstraintAttribute.END,
                relation: Gtk.ConstraintRelation.EQ,
                source: null,
                sourceAttribute: Gtk.ConstraintAttribute.END,
                multiplier: 1,
                constant: -8,
                strength: Gtk.ConstraintStrength.REQUIRED,
            })
        );
    }

    _addConstraintsEven() {
        // super.start = child.start - 8
        this.layoutManager.add_constraint(
            new Gtk.Constraint({
                target: null,
                targetAttribute: Gtk.ConstraintAttribute.START,
                relation: Gtk.ConstraintRelation.EQ,
                source: this.get_last_child(),
                sourceAttribute: Gtk.ConstraintAttribute.START,
                multiplier: 1,
                constant: -8,
                strength: Gtk.ConstraintStrength.REQUIRED,
            })
        );

        // child.end = guide.start
        this.layoutManager.add_constraint(
            new Gtk.Constraint({
                target: this.get_last_child(),
                targetAttribute: Gtk.ConstraintAttribute.END,
                relation: Gtk.ConstraintRelation.EQ,
                source: this._guide,
                sourceAttribute: Gtk.ConstraintAttribute.START,
                multiplier: 1,
                constant: 0,
                strength: Gtk.ConstraintStrength.REQUIRED,
            })
        );
    }

    _addConstraintFirst() {
        // super.top = child.top - 8
        this.layoutManager.add_constraint(
            new Gtk.Constraint({
                target: null,
                targetAttribute: Gtk.ConstraintAttribute.TOP,
                relation: Gtk.ConstraintRelation.EQ,
                source: this.get_first_child(),
                sourceAttribute: Gtk.ConstraintAttribute.TOP,
                multiplier: 1,
                constant: -8,
                strength: Gtk.ConstraintStrength.REQUIRED,
            })
        );
    }

    _addConstraintSibling() {
        // child.top = sibling.bottom
        this.layoutManager.add_constraint(
            new Gtk.Constraint({
                target: this.get_last_child(),
                targetAttribute: Gtk.ConstraintAttribute.TOP,
                relation: Gtk.ConstraintRelation.EQ,
                source: this.get_last_child().get_prev_sibling(),
                sourceAttribute: Gtk.ConstraintAttribute.BOTTOM,
                multiplier: 1,
                constant: 0,
                strength: Gtk.ConstraintStrength.REQUIRED,
            })
        );
    }

    _addConstraintLast() {
        // child.bottom = super.bottom - 8
        this.layoutManager.add_constraint(
            new Gtk.Constraint({
                target: this.get_last_child(),
                targetAttribute: Gtk.ConstraintAttribute.BOTTOM,
                relation: Gtk.ConstraintRelation.EQ,
                source: null,
                sourceAttribute: Gtk.ConstraintAttribute.BOTTOM,
                multiplier: 1,
                constant: -8,
                strength: Gtk.ConstraintStrength.REQUIRED,
            })
        );
    }
});
InteractiveGrid.set_layout_manager_type(Gtk.ConstraintLayout.$gtype);

let application = new Gtk.Application();

application.connect('activate', () => {
    let grid = new InteractiveGrid(
        ["Child 1", "Child 2", "Child 3"].map(label => new Gtk.Button({ label })),
        { hexpand: true, vexpand: true }
    );

    let box = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        spacing: 12,
    });
    box.append(grid);

    new Gtk.Window({
        application,
        title: "Interactive Constraints",
        defaultWidth: 260,
        child: box,
    }).present();
});

application.run([]);
