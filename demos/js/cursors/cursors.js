// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Cursors
 *
 * Demonstrates a useful set of available cursors. The cursors shown here are the
 * ones defined by CSS, which we assume to be available. The example shows creating
 * cursors by name or from an image, with or without a fallback.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');

let application = new Gtk.Application();

application.connect('activate', () => {
    let builder = Gtk.Builder.new_from_file(directory.get_child('cursors.ui').get_path());
    let window = builder.get_object('window');

    application.add_window(window);
    window.present();
});

application.run([]);
