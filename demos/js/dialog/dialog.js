// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Dialogs
 *
 * Dialogs are used to pop up transient windows for information
 * and user feedback.
 */

imports.gi.versions['Gtk'] = '4.0';
const { GObject, Gtk } = imports.gi;
const _ = imports.gettext.domain('gtk40').gettext;
const _n = imports.gettext.domain('gtk40').ngettext;
String.prototype.format = imports.format.format;

const Grid = GObject.registerClass(class DemoGrid extends Gtk.Grid {
    _init(params) {
        super._init(params);

        let label1 = Gtk.Label.new_with_mnemonic("_Entry 1");
        this.entry1 = new Gtk.Entry();
        label1.set_mnemonic_widget(this.entry1);
        this.attach(label1, 0, 0, 1, 1);
        this.attach(this.entry1, 1, 0, 1, 1);

        let label2 = Gtk.Label.new_with_mnemonic("E_ntry 2");
        this.entry2 = new Gtk.Entry();
        label2.set_mnemonic_widget(this.entry2);
        this.attach(label2, 0, 1, 1, 1);
        this.attach(this.entry2, 1, 1, 1, 1);
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        spacing: 8,
        marginStart: 8, marginEnd: 8,
        marginTop: 8, marginBottom: 8,
    });

    // Standard message dialog
    {
        let hbox = new Gtk.Box({ spacing: 8 });
        let button = Gtk.Button.new_with_mnemonic("_Message Dialog");
        hbox.append(button);

        let i = 0;
        button.connect('clicked', () => {
            i++;
            let dialog = new Gtk.MessageDialog({
                transientFor: button.root,
                modal: true, destroyWithParent: true,
                messageType: Gtk.MessageType.INFO,
                buttons: Gtk.ButtonsType.OK_CANCEL,
                text: "Test message",
                secondaryText: _n("Has been shown once", "Has been shown %d times", i).format(i),
            });
            dialog.connect('response', () => dialog.destroy());
            dialog.show();
        });

        vbox.append(hbox);
        vbox.append(new Gtk.Separator());
    }

    // Interactive dialog
    {
        let hbox = new Gtk.Box({ spacing: 8 });
        let vbox2 = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let button = Gtk.Button.new_with_mnemonic("_Interactive Dialog");
        vbox2.append(button);
        hbox.append(vbox2);

        let globalGrid = new Grid({
            rowSpacing: 4, columnSpacing: 4,
        });
        hbox.append(globalGrid);

        button.connect('clicked', () => {
            let localGrid = new Grid({
                rowSpacing: 6, columnSpacing: 6,
                halign: Gtk.Align.CENTER, valign: Gtk.Align.CENTER,
                hexpand: true, vexpand: true,
            });
            localGrid.entry1.text = globalGrid.entry1.text;
            localGrid.entry2.text = globalGrid.entry2.text;

            let dialog = new Gtk.Dialog({
                transientFor: button.root,
                modal: true, destroyWithParent: true, useHeaderBar: true,
                title: "Interactive Dialog",
            });
            dialog.add_button(_("_OK"), Gtk.ResponseType.OK);
            dialog.add_button(_("_Cancel"), Gtk.ResponseType.CANCEL);
            dialog.set_default_response(Gtk.ResponseType.OK);
            dialog.get_content_area().append(localGrid);
            dialog.connect('response', (dialog_, response) => {
                if (response == Gtk.ResponseType.OK) {
                    globalGrid.entry1.text = localGrid.entry1.text;
                    globalGrid.entry2.text = localGrid.entry2.text;
                }

                dialog.destroy();
            });
            dialog.show();
        });

        vbox.append(hbox);
    }

    new Gtk.Window({
        application,
        title: "Dialogs",
        resizable: false,
        child: vbox,
    }).present();
});

application.run([]);
