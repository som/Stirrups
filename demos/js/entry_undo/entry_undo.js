// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Entry/Undo and Redo
 *
 * GtkEntry can provide basic Undo/Redo support using standard keyboard
 * accelerators such as Control+z to undo and Control+Shift+z to redo.
 * Additionally, Control+y can be used to redo.
 */

imports.gi.versions['Gtk'] = '4.0';
const Gtk = imports.gi.Gtk;

let application = new Gtk.Application();

application.connect('activate', () => {
    let window = new Gtk.Window({
        application, title: "Undo and Redo",
        resizable: false,
        child: new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            marginTop: 18, marginBottom: 18,
            marginStart: 18, marginEnd: 18,
            spacing: 12,
        }),
    });

    window.child.append(new Gtk.Label({
        label: "Use Control+z or Control+Shift+z to undo or redo changes",
        useMarkup: true,
    }));
    // 'enable-undo' is a GtkEditable property.
    window.child.append(new Gtk.Entry({ enableUndo: true }));

    window.present();
});

application.run([]);
