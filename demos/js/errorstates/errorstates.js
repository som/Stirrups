// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Error States
 *
 * GtkLabel and GtkEntry can indicate errors if you set the .error
 * style class on them.
 *
 * This examples shows how this can be used in a dialog for input validation.
 *
 * It also shows how pass callbacks and objects to GtkBuilder with
 * GtkBuilderScope and gtk_builder_expose_object().
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GObject, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');

// GJS handles the "scope" machinery for widget class templates, whose an example is
// provided by the "Application Class" demo. Here is a generic builder scope to bind
// signal handlers manually. Contrary to the GJS one, it accepts swapped handlers.
const BuilderJSScope = GObject.registerClass({
    GTypeName: 'BuilderJSScope',
    Implements: [Gtk.BuilderScope],
}, class extends GObject.Object {
    vfunc_create_closure(builder, handlerName, flags, object) {
        if (!this._callbacks?.has(handlerName)) {
            logError(new Error(`No callback found for "${handlerName}" handler`));
            return null;
        }

        const callback = this._callbacks.get(handlerName);
        const swapped = flags & Gtk.BuilderClosureFlags.SWAPPED;

        return swapped ?
            (...args) => callback(object, ...args.slice(1), args[0]) :
            (...args) => callback(...args, object);
    }

    addCallbackSymbol(handlerName, callback) {
        (this._callbacks ?? (this._callbacks = new Map())).set(handlerName, callback);
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    // A useless window to suit to the original demonstration of object exposition.
    let toplevelWindow = new Gtk.Window({
        application,
        title: "Toplevel Window",
    });

    let scope = new BuilderJSScope();
    let builder = new Gtk.Builder({ scope });

    scope.addCallbackSymbol('validate_more_details', (moreDetailsEntry, pspec_, detailsEntry) => {
        if (moreDetailsEntry.text.length > 0 && !detailsEntry.text) {
            moreDetailsEntry.set_tooltip_text("Must have details first");
            moreDetailsEntry.add_css_class('error');
        } else {
            moreDetailsEntry.set_tooltip_text("");
            moreDetailsEntry.remove_css_class('error');
        }
    });

    scope.addCallbackSymbol('mode_switch_state_set', (modeSwitch, state, levelScale) => {
        if (!state || levelScale.get_value() > 50) {
            builder.get_object('error_label').hide();
            modeSwitch.set_state(state);
        } else {
            builder.get_object('error_label').show();
        }

        return true;
    });

    scope.addCallbackSymbol('level_scale_value_changed', (levelScale, modeSwitch) => {
        if (modeSwitch.active && !modeSwitch.state && levelScale.get_value() > 50) {
            builder.get_object('error_label').hide();
            modeSwitch.set_state(true);
        } else if (modeSwitch.state && levelScale.get_value() <= 50) {
            modeSwitch.set_state(false);
        }
    });

    builder.expose_object('toplevel', toplevelWindow);
    builder.add_from_file(directory.get_child('errorstates.ui').get_path());
    let dialog = builder.get_object('dialog');

    toplevelWindow.present();
    dialog.connect('response', () => {
        dialog.destroy();
        toplevelWindow.destroy();
    });
    dialog.show();
});

application.run([]);
