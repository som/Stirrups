// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Expander
 *
 * GtkExpander allows to provide additional content that is initially hidden.
 * This is also known as "disclosure triangle".
 *
 * This example also shows how to make the window resizable only if the expander
 * is expanded.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, Gio, Gtk } = imports.gi;

const LOGO_FILE = Gio.File.new_for_path('.').get_child('gtk_logo_cursor.png');
const TEXT =
`Finally, the full story with all details. And all the inside information, including error codes, etc etc. Pages of information, you might have to scroll down to read it all, or even resize the window - it works !
A second paragraph will contain even more innuendo, just to make you scroll down or resize the window.
Do it already!
`;

let application = new Gtk.Application();

application.connect('activate', () => {
    let tag = new Gtk.TextTag({
        pixelsAboveLines: 200,
        justification: Gtk.Justification.RIGHT,
    });
    let tagTable = new Gtk.TextTagTable();
    tagTable.add(tag);

    let buffer = new Gtk.TextBuffer({
        text: TEXT,
        tagTable,
    });

    let start = buffer.get_end_iter();
    buffer.insert_paintable(start, Gdk.Texture.new_from_file(LOGO_FILE));
    start.backward_char();

    buffer.apply_tag(tag, start, buffer.get_end_iter());

    let textView = new Gtk.TextView({
        buffer,
        editable: false,
        cursorVisible: false,
        wrapMode: Gtk.WrapMode.WORD,
        pixelsAboveLines: 2, pixelsBelowLines: 2,
        leftMargin: 10, rightMargin: 10,
        topMargin: 10, bottomMargin: 10,
    });

    let scrolledWindow = new Gtk.ScrolledWindow({
        child: textView,
        minContentHeight: 100,
        hasFrame: true,
        hscrollbarPolicy: Gtk.PolicyType.NEVER, vscrollbarPolicy: Gtk.PolicyType.AUTOMATIC,
        propagateNaturalHeight: true,
        vexpand: true,
    });

    let expander = new Gtk.Expander({
        child: scrolledWindow,
        label: "Details:",
        vexpand: true,
    });

    let dialog = new Gtk.MessageDialog({
        application,
        messageType: Gtk.MessageType.ERROR,
        buttons: Gtk.ButtonsType.CLOSE,
        useMarkup: true,
        text: "<big><b>Something went wrong</b></big>",
        secondaryText: "Here are some more details but not the full story.",
    });

    let label = dialog.messageArea.get_last_child();
    label.set_wrap(false);
    label.set_vexpand(false);
    dialog.messageArea.append(expander);

    expander.connect('notify::expanded', () => dialog.set_resizable(expander.expanded));
    dialog.connect('response', () => dialog.destroy());
    dialog.show();
});

application.run([]);
