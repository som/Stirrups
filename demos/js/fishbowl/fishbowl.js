// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Fishbowl
 *
 * This demo models the fishbowl demos seen on the web in a GTK way.
 * It's also a neat little tool to see how fast your computer (or
 * your GTK version) is.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gio, GLib, GObject, Gsk, Gtk } = imports.gi;
const { byteArray: ByteArray, format: Format } = imports;
void imports.fishbowlwidget;
const directory = Gio.File.new_for_path('.');

const css = `.blurred-button {
  box-shadow: 0px 0px 5px 10px rgba(0, 0, 0, 0.5);
}`;

const createFunctions = Object.values({
    Icon() {
        if (!this._iconNames)
            this._iconNames = Gtk.IconTheme.get_for_display(application.get_active_window().display).iconNames;

        let iconName = this._iconNames[GLib.random_int_range(0, this._iconNames.length)];
        return new Gtk.Image({ iconName, iconSize: Gtk.IconSize.LARGE });
    },

    Button() {
        return new Gtk.Button({ label: "Button" });
    },

    Blurbutton() {
        return new Gtk.Button({ cssClasses: ['blurred-button'] });
    },

    Fontbutton() {
        return new Gtk.FontButton();
    },

    Levelbar() {
        return new Gtk.LevelBar({ minValue: 0, maxValue: 100, value: 50, widthRequest: 200 });
    },

    Label() {
        return new Gtk.Label({
            label: "pLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
            wrap: true, maxWidthChars: 100,
        });
    },

    Spinner() {
        return new Gtk.Spinner({ spinning: true });
    },

    Spinbutton() {
        return Gtk.SpinButton.new_with_range(0, 10, 1);
    },

    Video() {
        return new Gtk.Video({
            widthRequest: 64, heightRequest: 64,
            loop: true, autoplay: true,
            file: directory.get_child('gtk-logo.webm'),
        });
    },

    // TODO: reuse Gtk.Gears from Gears demo
    /*Gears() {
        return new Gtk.Gears({ widthRequest: 100, heightRequest: 100 });
    },*/

    Switch() {
        return new Gtk.Switch({ state: true });
    },

    Menubutton() {
        let popover = new Gtk.Popover({ autohide: false, child: new Gtk.Label({ label: "Hey!" }) });
        let menuButton = new Gtk.MenuButton({ popover });
        menuButton.connect('map', () => {
            menuButton.popup();
        });

        return menuButton;
    },

    // TODO: reuse Gsk.ShaderPaintable from "Transitions and Effects" (gltransition) demo
    /*Shader() {
        if (!this._cogShader) {
            let [_success, bytes] = directory.get_child('cogs2.glsl').load_contents(null);
            this._cogShader = new Gsk.GLShader({ source: ByteArray.toGBytes(bytes) });
        }

        let paintable = new Gsk.ShaderPaintable({ shader: this._cogShader });
        let picture = new Gtk.Picture({ paintable, widthRequest: 100, heightRequest: 75 });

        picture.add_tick_callback((widget, frameClock) => {
            let frameTime = frameClock.get_frame_time();
            paintable.update_time(0, frameTime);
        });

        return picture;
    },*/
});

const Window = GObject.registerClass({
    GTypeName: 'FishbowlWindow',
    Template: directory.get_child('fishbowl.ui').get_uri(),
    InternalChildren: ['bowl'],
}, class extends Gtk.ApplicationWindow {
    _init(params) {
        this.fishbowl_prev_button_clicked_cb = this._fishbowl_prev_button_clicked_cb.bind(this);
        this.fishbowl_next_button_clicked_cb = this._fishbowl_next_button_clicked_cb.bind(this);

        super._init(params);

        this.widgetType = 0;
    }

    get widgetType() {
        return this._widgetType;
    }

    set widgetType(widgetType) {
        if (widgetType === this._widgetType)
            return;

        this._widgetType = widgetType;

        this._bowl.createFunction = createFunctions[widgetType];
        this.title = createFunctions[widgetType].name;
    }

    format_header_cb(window_, count, fps) {
        let string = count == 1 ? '%d Icon, %.2f fps' : '%d Icons, %.2f fps';
        return Format.format.call(string, count, fps);
    }

    fishbowl_changes_toggled_cb(button) {
        button.iconName = button.active ? 'changes-prevent' : 'changes-allow';
    }

    _fishbowl_prev_button_clicked_cb() {
        this.widgetType = this.widgetType == 0 ? createFunctions.length - 1 : this.widgetType - 1;
    }

    _fishbowl_next_button_clicked_cb() {
        this.widgetType = this.widgetType == createFunctions.length - 1 ? 0 : this.widgetType + 1;
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let window = new Window({ application });

    let provider = new Gtk.CssProvider();
    provider.load_from_data(css);
    Gtk.StyleContext.add_provider_for_display(window.display, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

    window.present();
});

application.run([]);
