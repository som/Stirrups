// SPDX-FileCopyrightText: 2017 Benjamin Otte <otte@gnome.org>
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

const { Gdk, GLib, GObject, Gtk } = imports.gi;
const System = imports.system;

// XXX: Workaround for the "offending callback" GJS critical error.
Gtk.Widget.prototype.addTickCallbackOld = Gtk.Widget.prototype.add_tick_callback;
Gtk.Widget.prototype.add_tick_callback = function(callback) {
    System.gc();
    let time = Date.now();

    return this.addTickCallbackOld(function(...args) {
        // Anticipate the next garbage collection to prevent tick
        // callbacks from "appearing" during a garbage collection.
        if ((Date.now() - time) >= 6900) {
            System.gc();
            time = Date.now();
        }

        return callback(...args);
    });
};

const getNewSpeed = function() {
    // 5s to 50s to cross screen seems fair
    return GLib.random_double_range(0.02, 0.2);
};

GObject.registerClass({
    GTypeName: 'FishbowlWidget',
    Properties: {
        'animating': GObject.ParamSpec.boolean(
            'animating', 'Animating', 'Whether children are moving around',
            GObject.ParamFlags.READWRITE, false
        ),
        'benchmark': GObject.ParamSpec.boolean(
            'benchmark', 'Benchmark', 'Adapt the count property to hit the maximum framerate',
            GObject.ParamFlags.READWRITE, false
        ),
        'count': GObject.ParamSpec.uint(
            'count', 'Count', 'Number of widgets',
            GObject.ParamFlags.READWRITE, 0, GLib.MAXUINT32, 0
        ),
        'framerate': GObject.ParamSpec.double(
            'framerate', 'Framerate', 'Framerate of this widget in frames per second',
            GObject.ParamFlags.READABLE, 0, Number.MAX_SAFE_INTEGER, 0
        ),
        'update-delay': GObject.ParamSpec.int64(
            'update-delay', 'Update delay', 'Number of usecs between updates',
            GObject.ParamFlags.READWRITE, 0, Number.MAX_SAFE_INTEGER, GLib.USEC_PER_SEC
        ),
    },
}, class extends Gtk.Widget {
    _init(params) {
        // TODO: Report issue to GJS: Init params contains 3 keys for the same 'css-name' property.
        params = Object.assign({}, params);
        delete params.cssName;
        delete params.css_name;

        super._init(params);

        this._children = new Map();
        this.accessibleRole = Gtk.AccessibleRole.PRESENTATION;
    }

    get animating() {
        return !!this._tickId;
    }

    set animating(animating) {
        if (animating == !!this._tickId)
            return;

        if (animating) {
            this._lastFrameTime = 0;
            this._tickId = this.add_tick_callback(this._onTick.bind(this));
        } else {
            this.remove_tick_callback(this._tickId);
            this._tickId = 0;
            this._framerate = 0;
            this.notify('framerate');
        }

        this.notify('animating');
    }

    get benchmark() {
        return this._benchmark || false;
    }

    set benchmark(benchmark) {
        if (benchmark == this._benchmark)
            return;

        if (!benchmark)
            this._lastBenchmarkChange = 0;

        this._benchmark = benchmark;
        this.notify('benchmark');
    }

    get count() {
        return this._children.size;
    }

    set count(count) {
        if (count == this.count)
            return;

        this.freeze_notify();

        while (count < this.count)
            this.remove(this.get_first_child());

        while (count > this.count)
            this.add(this._createNewChild());

        this.thaw_notify();
    }

    get framerate() {
        return this._framerate || 0;
    }

    _doUpdate() {
        let frameClock = this.get_frame_clock();
        if (!frameClock)
            return;

        let startCounter = frameClock.get_history_start();
        let endCounter = frameClock.get_frame_counter();
        let start = frameClock.get_timings(startCounter);
        let end = frameClock.get_timings(endCounter);
        while (endCounter > startCounter && end && !end.get_complete()) {
            endCounter--;
            end = frameClock.get_timings(endCounter);
        }
        if (endCounter - startCounter < 4)
            return;

        let startTimestamp = start.get_presentation_time();
        let endTimestamp = end.get_presentation_time();
        if (startTimestamp == 0 || endTimestamp == 0) {
            startTimestamp = start.get_frame_time();
            endTimestamp = end.get_frame_time();
        }

        let nFrames = endCounter - startCounter;
        this._framerate = nFrames * GLib.USEC_PER_SEC / (endTimestamp - startTimestamp);
        this._framerate = Math.round(this._framerate * 100) / 100;
        this.notify('framerate');

        if (!this.benchmark)
            return;

        let interval = end.get_refresh_interval();

        // Guess refresh interval
        if (interval == 0) {
            interval = GLib.MAXINT64;
            for (let i = startCounter; i < endCounter; i++) {
                let t = frameClock.get_timings(i);
                let before = frameClock.get_timings(i - 1);
                if (!t || !before)
                    continue;

                let ts = t.get_frame_time();
                let beforeTs = before.get_frame_time();
                if (ts == 0 || beforeTs == 0)
                    continue;

                interval = Math.min(interval, ts - beforeTs);
            }

            if (interval == GLib.MAXINT64)
                return;
        }

        let expectedFrames = Math.round((endTimestamp - startTimestamp) / interval);

        if (nFrames >= expectedFrames)
            this._lastBenchmarkChange = this._lastBenchmarkChange > 0 ? 2 * this._lastBenchmarkChange : 1;
        else if (nFrames + 1 < expectedFrames)
            this._lastBenchmarkChange = this._lastBenchmarkChange < 0 ? this._lastBenchmarkChange - 1 : -1;
        else
            this._lastBenchmarkChange = 0;

        this.count = Math.max(1, this.count + this._lastBenchmarkChange);
    }

    _onTick(fishbowl_, frameClock) {
        let frameTime = frameClock.get_frame_time();
        let elapsed = frameTime - this._lastFrameTime;
        let doUpdate = parseInt(frameTime / this.updateDelay) != parseInt(this._lastFrameTime / this.updateDelay);
        this._lastFrameTime = frameTime;

        // last frame was 0, so we're just starting to animate
        if (elapsed == frameTime)
            return GLib.SOURCE_CONTINUE;

        this._children.forEach((info, child) => {
            info.x += info.dx * elapsed / GLib.USEC_PER_SEC;
            info.y += info.dy * elapsed / GLib.USEC_PER_SEC;

            if (info.x <= 0)
                Object.assign(info, { x: 0, dx: getNewSpeed() });
            else if (info.x >= 1)
                Object.assign(info, { x: 1, dx: -getNewSpeed() });

            if (info.y <= 0)
                Object.assign(info, { y: 0, dy: getNewSpeed() });
            else if (info.y >= 1)
                Object.assign(info, { y: 1, dy: -getNewSpeed() });
        });

        this.queue_allocate();
        if (doUpdate)
            this._doUpdate();

        return GLib.SOURCE_CONTINUE;
    }

    vfunc_unrealize() {
        this.count = 0;
        this.animating = false;

        super.vfunc_unrealize();
    }

    vfunc_measure(orientation, forSize_) {
        let [minimum, natural] = [0, 0];

        this._children.forEach((info_, child) => {
            let childMin, childNat;

            if (orientation == Gtk.Orientation.HORIZONTAL) {
                [childMin, childNat] = child.measure(orientation, -1);
            } else {
                let [minWidth] = child.measure(Gtk.Orientation.HORIZONTAL, -1);
                [childMin, childNat] = child.measure(orientation, minWidth);
            }

            minimum = Math.max(minimum, childMin);
            natural = Math.max(natural, childNat);
        });

        return [minimum, natural, -1, -1];
    }

    vfunc_size_allocate(width, height, baseline) {
        this._children.forEach((info, child) => {
            let [childMinReq] = child.get_preferred_size();
            let childAlloc = new Gdk.Rectangle({
                x: Math.round(info.x * (width - childMinReq.width)),
                y: Math.round(info.y * (height - childMinReq.height)),
                width: childMinReq.width,
                height: childMinReq.height
            });

            child.size_allocate(childAlloc, -1);
        });
    }

    add(widget) {
        if (!(widget instanceof Gtk.Widget))
            throw new Error(`${widget} is not a widget`);

        widget.set_parent(this);
        widget.update_state([Gtk.AccessibleState.HIDDEN], [true]);

        let info = {
            x: 0, y: 0,
            dx: getNewSpeed(), dy: getNewSpeed(),
        };

        this._children.set(widget, info);
        this.notify('count');
    }

    remove(widget) {
        widget.unparent();

        if (!this._children.delete(widget))
            throw new Error(`${widget} is not a child of ${this}`);

        this.notify('count');
    }

    set createFunction(createFunction) {
        this.freeze_notify();

        this.count = 0;
        this._lastBenchmarkChange = 0;

        this._createNewChild = createFunction;
        this.count = 1;

        this.thaw_notify();
    }
});
