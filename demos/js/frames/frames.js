// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Frames
 *
 * This demo is intentionally as simple as possible, to see what
 * framerate the windowing system can deliver on its own.
 *
 * It does nothing but change the drawn color, for every frame.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, Gio, GLib, GObject, Graphene, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');
const vprintf = imports.format.vprintf;

const TIME_SPAN = 3 * GLib.TIME_SPAN_SECOND; // 3s expressed in µs.

const ColorWidget = GObject.registerClass(class ColorWidget extends Gtk.Widget {
    _init(params) {
        this._color1 = { red: 0, green: 0, blue: 0 };
        this._color2 = { red: 0, green: 0, blue: 0 };
        this._timeNext = 0;
        this._t = 0;

        super._init(Object.assign({
            hexpand: true,
            vexpand: true,
        }, params));

        this.add_tick_callback(() => {
            this._changeColor();
            return GLib.SOURCE_CONTINUE;
        });
    }

    _changeColor() {
        let time = this.get_frame_clock().get_frame_time();

        if (time >= this._timeNext) {
            this._timeNext = time + TIME_SPAN;

            this._color1 = this._color2;
            this._color2 = {
                red: Math.random(),
                green: Math.random(),
                blue: Math.random(),
            };
        }

        this._t = 1 - (this._timeNext - time) / TIME_SPAN;
        this.queue_draw();
    }

    vfunc_snapshot(snapshot) {
        snapshot.append_color(new Gdk.RGBA({
            red: (1 - this._t) * this._color1.red + this._t * this._color2.red,
            green: (1 - this._t) * this._color1.green + this._t * this._color2.green,
            blue: (1 - this._t) * this._color1.blue + this._t * this._color2.blue,
            alpha: 1,
        }), new Graphene.Rect({
            size: { width: this.get_width(), height: this.get_height() },
        }));
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let builder = Gtk.Builder.new_from_file(directory.get_child('frames.ui').get_path());

    builder.get_object('box').append(new ColorWidget());

    let label = builder.get_object('fps');
    GLib.timeout_add(GLib.PRIORITY_DEFAULT, 500, () => {
        let frameClock = label.get_frame_clock();
        label.label = frameClock ? vprintf("%.2f fps", [frameClock.get_fps()]) : "";

        return GLib.SOURCE_CONTINUE;
    });

    let window = builder.get_object('window');
    window.set_application(application);
    window.present();
});

application.run([]);
