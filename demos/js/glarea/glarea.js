// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* OpenGL Area
 *
 * GtkGLArea is a widget that allows custom drawing using OpenGL calls.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GObject, Gtk, Updwn: gl } = imports.gi;
const ByteArray = imports.byteArray;
const directory = Gio.File.new_for_path('.');

const Axis = { X: 0, Y: 1, Z: 2 };

const vertexData = [
     0.0,  0.500, 0, 1,
     0.5, -0.366, 0, 1,
    -0.5, -0.366, 0, 1,
];

const Area = GObject.registerClass(class _Area extends Gtk.GLArea {
    _init(params) {
        super._init(params);
        this.rotationAngles = [];
    }

    // Compute the model view projection matrix using the
    // rotation angles specified through the GtkRange widgets.
    get _mvp() {
        let [phi, theta, psi] = this.rotationAngles;

        let x = phi * (Math.PI / 180);
        let y = theta * (Math.PI / 180);
        let z = psi * (Math.PI / 180);
        let c1 = Math.cos(x), s1 = Math.sin(x);
        let c2 = Math.cos(y), s2 = Math.sin(y);
        let c3 = Math.cos(z), s3 = Math.sin(z);
        let c3c2 = c3 * c2;
        let s3c1 = s3 * c1;
        let c3s2s1 = c3 * s2 * s1;
        let s3s1 = s3 * s1;
        let c3s2c1 = c3 * s2 * c1;
        let s3c2 = s3 * c2;
        let c3c1 = c3 * c1;
        let s3s2s1 = s3 * s2 * s1;
        let c3s1 = c3 * s1;
        let s3s2c1 = s3 * s2 * c1;
        let c2s1 = c2 * s1;
        let c2c1 = c2 * c1;

        /* The identity matrix:
         * ⎡ 1 0 0 0 ⎤
         * ⎢ 0 1 0 0 ⎥
         * ⎢ 0 0 1 0 ⎥
         * ⎣ 0 0 0 1 ⎦
         */

        /* Apply all three rotations using the three matrices:
         *
         * ⎡  c3 s3 0 ⎤ ⎡ c2  0 -s2 ⎤ ⎡ 1  00 00 ⎤
         * ⎢  s3 c3 0 ⎥ ⎢ 00  1  00 ⎥ ⎢ 0  c1 s1 ⎥
         * ⎣  00 00 1 ⎦ ⎣ s2  0  c2 ⎦ ⎣ 0 -s1 c1 ⎦
         */

        let mvp = new Array(16);
        mvp[0] = c3c2;  mvp[4] = s3c1 + c3s2s1;  mvp[8] = s3s1 - c3s2c1; mvp[12] = 0;
        mvp[1] = -s3c2; mvp[5] = c3c1 - s3s2s1;  mvp[9] = c3s1 + s3s2c1; mvp[13] = 0;
        mvp[2] = s2;    mvp[6] = -c2s1;         mvp[10] = c2c1;          mvp[14] = 0;
        mvp[3] = 0;     mvp[7] = 0;             mvp[11] = 0;             mvp[15] = 1;

        return mvp;
    }

    _initBuffers() {
        // We only use one VAO, so we always keep it bound.
        [this._vao] = gl.genVertexArrays(1);
        gl.bindVertexArray(this._vao);

        // This is the buffer that holds the vertices.
        [this._vertexBuffer] = gl.genBuffers(1);
        gl.bindBuffer(gl.ARRAY_BUFFER, this._vertexBuffer);
        gl.bufferDatafv(gl.ARRAY_BUFFER, vertexData, gl.STATIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, 0);
    }

    // Create and compile a shader.
    _createShader(shaderType, file) {
        let [success_, contents] = file.load_contents(null);
        let shader = gl.createShader(shaderType);
        gl.shaderSource(shader, [ByteArray.toString(contents)], null);
        gl.compileShader(shader);

        if (!gl.getShaderiv(shader, gl.COMPILE_STATUS)[0]) {
            let infoLog = gl.getShaderInfoLog(shader, -1);
            let infoType = shaderType == gl.VERTEX_SHADER ? 'vertex' : 'fragment';
            gl.deleteShader(shader);
            throw new Error(`Compile failure in ${infoType} shader:\n${ByteArray.toString(infoLog)}`);
        }

        return shader;
    }

    // Initialize the shaders and link them into a program.
    _initShaders(vertexFile, fragmentFile) {
        let vertex, fragment;

        try {
            vertex = this._createShader(gl.VERTEX_SHADER, vertexFile);
            fragment = this._createShader(gl.FRAGMENT_SHADER, fragmentFile);

            // link the vertex and fragment shaders together
            let program = gl.createProgram();
            gl.attachShader(program, vertex);
            gl.attachShader(program, fragment);
            gl.linkProgram(program);

            if (!gl.getProgramiv(program, gl.LINK_STATUS)[0]) {
                let infoLog = gl.getProgramInfoLog(program, -1);
                gl.deleteProgram(program);
                throw new Error(`Linking failure:\n${ByteArray.toString(infoLog)}`);
            }

            gl.detachShader(program, vertex);
            gl.detachShader(program, fragment);
            this._program = program;
            // Get the location of the "mvp" uniform
            this._mvpLocation = gl.getUniformLocation(this._program, 'mvp');
        } catch(e) {
            logError(e);
        }

        if (vertex)
            gl.deleteShader(vertex);
        if (fragment)
            gl.deleteShader(fragment);
    }

    // We need to set up our state when we realize the GtkGLArea widget.
    vfunc_realize() {
        super.vfunc_realize();

        this.make_current();
        if (this.get_error())
            return;

        let [vertexFile, fragmentFile] = this.get_context().get_use_es() ?
            [directory.get_child('glarea-gles.vs.glsl'), directory.get_child('glarea-gles.fs.glsl')] :
            [directory.get_child('glarea-gl.vs.glsl'), directory.get_child('glarea-gl.fs.glsl')];

        this._initShaders(vertexFile, fragmentFile);
        this._initBuffers();
    }

    // We should tear down the state when unrealizing.
    vfunc_unrealize() {
        this.make_current();
        if (!this.get_error()) {
            gl.deleteBuffers([this._vertexBuffer]);
            gl.deleteVertexArrays([this._vao]);
            gl.deleteProgram(this._program);
        }
        super.vfunc_unrealize();
    }

    vfunc_render(context) {
        if (this.get_error())
            return false;

        // Clear the viewport.
        gl.clearColor(0.5, 0.5, 0.5, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT);

        // Use our shaders.
        gl.useProgram(this._program);

        // Update the "mvp" matrix we use in the shader.
        gl.uniformMatrix4fv(this._mvpLocation, false, this._mvp);

        // Use the vertices in our buffer.
        gl.bindBuffer(gl.ARRAY_BUFFER, this._vertexBuffer);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0, 4, gl.FLOAT, false, 0, 0);

        // Draw the three vertices as a triangle.
        gl.drawArrays(gl.TRIANGLES, 0, 3);

        // We finished using the buffers and program.
        gl.disableVertexAttribArray(0);
        gl.bindBuffer(gl.ARRAY_BUFFER, 0);
        gl.useProgram(0);

        // Flush the contents of the pipeline.
        gl.flush();

        return true;
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let window = new Gtk.Window({
        application,
        title: "OpenGL Area",
        defaultWidth: 400, defaultHeight: 600,
    });

    let vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        spacing: 6,
        marginStart: 12, marginEnd: 12,
        marginTop: 12, marginBottom: 12,
    });
    window.set_child(vbox);

    let area = new Area({
        hexpand: true, vexpand: true,
        widthRequest: 100, heightRequest: 200,
    });
    vbox.append(area);

    let controls = new Gtk.Box({ hexpand: true });
    vbox.append(controls);
    Object.values(Axis).forEach(axis => {
        let label = (axis == Axis.X ? 'X ' : axis == Axis.Y ? 'Y' : 'Z') + " axis";
        let adjustment = Gtk.Adjustment.new(0, 0, 360, 1, 12, 0);
        adjustment.connect('value-changed', () => {
            area.rotationAngles[axis] = adjustment.value;
            area.queue_draw();
        });
        area.rotationAngles[axis] = adjustment.value;

        let hbox = new Gtk.Box();
        hbox.append(new Gtk.Label({ label }));
        hbox.append(new Gtk.Scale({ adjustment, hexpand: true }));
        vbox.append(hbox);
    });

    let button = new Gtk.Button({ hexpand: true, label: "Quit" });
    vbox.append(button);
    button.connect('clicked', () => window.destroy());

    window.present();
});

application.run([]);
