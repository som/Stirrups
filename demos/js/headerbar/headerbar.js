// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Header Bar
 *
 * GtkHeaderBar is a container that is suitable for implementing
 * window titlebars. One of its features is that it can position
 * a title centered with regard to the full width, regardless of
 * variable-width content at the left or right.
 *
 * It is commonly used with gtk_window_set_titlebar()
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gtk } = imports.gi;

let application = new Gtk.Application();

application.connect('activate', () => {
    let box = new Gtk.Box();
    box.append(new Gtk.Button({ iconName: 'go-previous-symbolic' }));
    box.append(new Gtk.Button({ iconName: 'go-next-symbolic' }));

    let headerBar = new Gtk.HeaderBar();
    headerBar.pack_start(box);
    headerBar.pack_start(new Gtk.Switch());
    headerBar.pack_end(new Gtk.Button({ iconName: 'mail-send-receive-symbolic' }));

    let window = new Gtk.Window({
        application,
        title: "Header Bar",
        defaultWidth: 600, defaultHeight: 600,
        child: new Gtk.TextView(),
    });
    window.set_titlebar(headerBar);
    window.present();
});

application.run([]);
