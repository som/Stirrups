// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Hypertext
 *
 * Usually, tags modify the appearance of text in the view, e.g. making it
 * bold or colored or underlined. But tags are not restricted to appearance.
 * They can also affect the behavior of mouse and key presses, as this demo
 * shows.
 *
 * We also demonstrate adding other things to a text view, such as
 * clickable icons.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, GLib, GObject, Gtk, Pango } = imports.gi;
const System = imports.system;

const PANGO_SCALE_X_LARGE = 1.44;

const WikiPage = {
    HOME: {},
    HYPERTEXT: {
        title: "hypertext",
        pronunciation: " / ˈhaɪ pərˌtɛkst / ",
        description: (
            `Machine-readable text that is not sequential but is organized so that related items of information are connected.`
        ),
    },
    TAG: {
        title: "tag",
        pronunciation: " / tag / ",
        description: (
            `An attribute that can be applied to some range of text. For example, a tag might be called “bold” and make the text inside the tag bold.
However, the tag concept is more general than that; tags don't have to affect appearance. They can instead affect the behavior of mouse and key presses, “lock” a range of text so the user can't edit it, or countless other things.`
        ),
    },
};

// Quick-and-dirty text-to-speech for a single word. If you don't hear anything,
// you are missing espeak and espeak-ng on your system.
const pronunciate = function(word) {
    try {
        GLib.spawn_async(null, ['espeak-ng', word], null, GLib.SpawnFlags.SEARCH_PATH, null);
    } catch(e) {
        try {
            GLib.spawn_async(null, ['espeak', word], null, GLib.SpawnFlags.SEARCH_PATH, null);
        } catch(e) {
            log("For word pronunciation, install the “espeak” or “espeak-ng” program");
        }
    }
};

// A map wrapper that behaves on the key side like a weak map, but with GObject instances.
// Indeed, the JS object lifetime is brief, unless a reference is explicitly keeped.
// On the contrary, System.addressOfGObject will return a constant value until the
// underlying GObject object is finalized.
// This is useful to attach data to GObject objects without storing the related JS objects.
// Be aware the map values (data) are never garbage-collected.
// Note that only the demo-required methods are implemented.
const GObjectWeakMap = class extends Map {
    get(key) {
        if (!(key instanceof GObject.Object))
            return undefined;

        return super.get(System.addressOfGObject(key));
    }

    set(key, value) {
        if (!(key instanceof GObject.Object))
            throw new Error(`${key} is not a ${GObject.Object.$gtype.name} instance`);

        super.set(System.addressOfGObject(key), value);
    }
};

// JS implementation of g_object_set_data and g_object_get_data.
GObject.Object.dataMap = new GObjectWeakMap();
GObject.Object.prototype.setData = function(key, data) {
    let value = GObject.Object.dataMap.get(this) ?? {};
    value[key] = data;
    GObject.Object.dataMap.set(this, value);
};
GObject.Object.prototype.getData = function(key) {
    return GObject.Object.dataMap.get(this)?.[key] ?? null;
};

// Make 'widget' property writable at controller initialization.
Gtk.EventController.prototype._eventControllerInitOld = Gtk.EventController.prototype._init;
Gtk.EventController.prototype._init = function(params = {}) {
    let widget = params.widget;
    delete params.widget;

    this._eventControllerInitOld(params);

    if (widget)
        widget.add_controller(this);
};

// gtk_text_buffer_insert_with_tags is not introspectable.
Gtk.TextBuffer.prototype.insertWithTags = function(iter, text, length, ...tags) {
    let mark = this.create_mark(null, iter, true);
    this.insert(iter, text, length);
    tags.forEach(tag => this.apply_tag(tag, this.get_iter_at_mark(mark), iter));
    this.delete_mark(mark);
};

const WikiTextBuffer = GObject.registerClass(class WikiTextBuffer extends Gtk.TextBuffer {
    getLinkedPageAtIter(iter) {
        return iter.get_tags().find(tag => !!tag.getData('page'))?.getData('page');
    }

    insertCode(iter, text, length) {
        let tag = new Gtk.TextTag({
            family: 'monospace',
        });

        this.tagTable.add(tag);
        this.insertWithTags(iter, text, length, tag);
    }

    // Inserts a piece of text into the buffer, giving it the usual appearance of a hyperlink
    // in a web browser: blue and underlined. Additionally, attaches some data on the tag, to
    // make it recognizable as a link.
    insertPageLink(iter, text, length, page) {
        let tag = new Gtk.TextTag({
            foreground: 'blue',
            underline: Pango.Underline.SINGLE,
        });

        tag.setData('page', page);
        this.tagTable.add(tag);
        this.insertWithTags(iter, text, length, tag);
    }

    insertTitle(iter, text, length) {
        let tag = new Gtk.TextTag({
            weight: Pango.Weight.BOLD,
            scale: PANGO_SCALE_X_LARGE,
        });

        this.tagTable.add(tag);
        this.insertWithTags(iter, text, length, tag);
    }
});

const WikiTextView = GObject.registerClass(class WikiTextView extends Gtk.TextView {
    _init(params) {
        super._init(params);

        // Links can be activated by pressing Enter.
        new Gtk.EventControllerKey({ widget: this }).connect('key-pressed', (controller_, keyval) => {
            switch (keyval) {
                case Gdk.KEY_Return:
                case Gdk.KEY_KP_Enter: {
                    let iter = this.buffer.get_iter_at_mark(this.buffer.get_insert());
                    let page = this.buffer.getLinkedPageAtIter(iter);

                    if (page) {
                        this._showPage(page);
                        return Gdk.EVENT_STOP;
                    }
                }
            }

            return Gdk.EVENT_PROPAGATE;
        });

        new Gtk.GestureClick({ widget: this }).connect('released', (gesture_, nPress, x, y) => {
            // Not to follow a link if the user has selected something.
            if (this.buffer.get_has_selection())
                return;

            [x, y] = this.window_to_buffer_coords(Gtk.TextWindowType.WIDGET, x, y);
            let [success, iter] = this.get_iter_at_location(x, y);
            let page = success && this.buffer.getLinkedPageAtIter(iter);

            if (page)
                this._showPage(page);
        });

        // Looks at all tags covering the position (x, y) in the text view, and if one of them is
        // a link, change the cursor to the "hands" cursor typically used by web browsers.
        new Gtk.EventControllerMotion({ widget: this }).connect('motion', (controller_, x, y) => {
            [x, y] = this.window_to_buffer_coords(Gtk.TextWindowType.WIDGET, x, y);
            let [success, iter] = this.get_iter_at_location(x, y);
            let hoveringOverLink = success && !!this.buffer.getLinkedPageAtIter(iter);

            if (this._hoveringOverLink != hoveringOverLink) {
                this._hoveringOverLink = hoveringOverLink;
                this.set_cursor_from_name(hoveringOverLink ? 'pointer' : 'text');
            }
        });

        this._showPage(WikiPage.HOME);
    }

    _appendHome() {
        let iter = this.buffer.get_iter_at_offset(0);
        this.buffer.insert(iter, "Some text to show that simple ", -1);
        this.buffer.insertPageLink(iter, "hypertext", -1, WikiPage.HYPERTEXT);
        this.buffer.insert(iter, " can easily be realized with ", -1);
        this.buffer.insertPageLink(iter, "tags", -1, WikiPage.TAG);
        this.buffer.insert(iter, "\n", -1);
        this.buffer.insert(iter, "Of course you can also embed Emoji 😋, icons ", -1);
        this.buffer.insert_paintable(iter, Gtk.IconTheme.get_for_display(this.get_display()).lookup_icon(
            "microphone-sensitivity-high-symbolic", null, 16, 1, Gtk.TextDirection.LTR, 0
        ));
        this.buffer.insert(iter, ", or even widgets ", -1);
        let widget = new Gtk.LevelBar({ minValue: 0, value: 50, maxValue: 100, widthRequest: 100 });
        this.add_child_at_anchor(widget, this.buffer.create_child_anchor(iter));
        this.buffer.insert(iter, ".", -1);
    }

    _appendPage(page) {
        let iter = this.buffer.get_iter_at_offset(0);
        this.buffer.insertTitle(iter, page.title, -1);
        this.buffer.insertCode(iter, page.pronunciation, -1);
        let widget = new Gtk.Image({ iconName: 'audio-volume-high-symbolic', cursor: new Gdk.Cursor({ name: 'pointer' }) });
        new Gtk.GestureClick({ widget }).connect('pressed', () => pronunciate(page.title));
        this.add_child_at_anchor(widget, this.buffer.create_child_anchor(iter));
        this.buffer.insert(iter, "\n", -1);
        this.buffer.insert(iter, page.description, -1);
        this.buffer.insert(iter, "\n", -1);
        this.buffer.insertPageLink(iter, "Go back", -1, WikiPage.HOME);
    }

    // Fills the buffer with text and interspersed links.
    _showPage(page) {
        this.buffer.text = '';
        this.buffer.begin_irreversible_action();

        if (page == WikiPage.HOME)
            this._appendHome();
        else
            this._appendPage(page);

        this.buffer.end_irreversible_action();
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    new Gtk.Window({
        application, title: "Hypertext",
        defaultWidth: 330, defaultHeight: 330, resizable: false,
        child: new Gtk.ScrolledWindow({
            child: new WikiTextView({
                wrapMode: Gtk.WrapMode.WORD,
                topMargin: 20, bottomMargin: 20,
                leftMargin: 20, rightMargin: 20,
                pixelsBelowLines: 10,
                buffer: new WikiTextBuffer({ enableUndo: true }),
            }),
        }),
    }).present();
});

application.run([]);
