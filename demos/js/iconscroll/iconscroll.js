// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Scrolling
 *
 * This demo scrolls a view with various content.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GLib, GObject, Gtk } = imports.gi;
const ByteArray = imports.byteArray;
const directory = Gio.File.new_for_path('.');
const vprintf = imports.format.vprintf;

const populateFunctions = Object.values({
    "Scrolling icons": function() {
        let grid = new Gtk.Grid({
            halign: Gtk.Align.CENTER,
            marginStart: 10, marginEnd: 10, marginTop: 10, marginBottom: 10,
            rowSpacing: 10, columnSpacing: 10,
        });

        for (let top = 0; top < 100; top++)
            for (let left = 0; left < 15; left++) {
                if (!this._iconNames)
                    this._iconNames = Gtk.IconTheme.get_for_display(application.get_active_window().display).iconNames;

                let iconName = this._iconNames[GLib.random_int_range(0, this._iconNames.length)];
                let image = new Gtk.Image({ iconName, iconSize: Gtk.IconSize.LARGE });
                grid.attach(image, left, top, 1, 1);
            }

        return { child: grid, hscrollbarPolicy: Gtk.PolicyType.NEVER, hincrement: 0, vincrement: 5 };
    },

    "Scrolling plain text": function() {
        let contents = directory.get_child('iconscroll.js').load_contents(null)[1];
        let textView = new Gtk.TextView();
        textView.buffer.set_text(ByteArray.toString(contents), -1);

        return { child: textView, hscrollbarPolicy: Gtk.PolicyType.NEVER, hincrement: 0, vincrement: 5 };
    },

    "Scrolling text with Emoji": function() {
        let string = "";
        for (let i = 0; i < 500; i++) {
            if (i % 2)
                string += "<span underline=\"single\" underline_color=\"red\">x</span>";

            for (let j = 0; j < 30; j++)
                string += "💓<span underline=\"single\" underline_color=\"red\">x</span>";

            string += "\n";
        }

        let textView = new Gtk.TextView();
        textView.buffer.insert_markup(textView.buffer.get_start_iter(), string, -1);

        return { child: textView, hscrollbarPolicy: Gtk.PolicyType.NEVER, hincrement: 0, vincrement: 5 };
    },

    "Scrolling a big image": function() {
        let image = new Gtk.Picture({
            file: directory.get_child('portland-rose.jpg'),
            canShrink: false,
        });

        return { child: image, hscrollbarPolicy: Gtk.PolicyType.AUTOMATIC, hincrement: 5, vincrement: 5 };
    },

    // TODO: Reuse the list view from Lists/Weather demo.
    /*"Scrolling a list": function() {
        let listView = create_weather_view();

        return { child: listView, hscrollbarPolicy: Gtk.PolicyType.AUTOMATIC, hincrement: 5, vincrement: 0 };
    },*/

    // TODO: Reuse the column view from Lists/Characters demo.
    /*"Scrolling a columned list": function() {
        let columnView = create_ucd_view(null);

        return { child: columnView, hscrollbarPolicy: Gtk.PolicyType.AUTOMATIC, hincrement: 5, vincrement: 0 };
    },*/

    // TODO: Reuse the grid view from Lists/Colors demo.
    /*"Scrolling a grid": function() {
        let gridView = create_color_grid();

        return { child: gridView, hscrollbarPolicy: Gtk.PolicyType.AUTOMATIC, hincrement: 5, vincrement: 0 };
    },*/
});

const Window = GObject.registerClass({
    GTypeName: 'IconscrollWindow',
    Template: directory.get_child('iconscroll.ui').get_uri(),
    InternalChildren: ['fps_label', 'scrolledwindow'],
}, class extends Gtk.ApplicationWindow {
    _init(params) {
        super._init(params);

        this.widgetType = 0;
        GLib.timeout_add(GLib.PRIORITY_DEFAULT, 500, () => {
            this._updateFps();
            return GLib.SOURCE_CONTINUE;
        });
    }

    _updateAdjustment() {
        [this._scrolledwindow.hadjustment, this._scrolledwindow.vadjustment].forEach(adjustment => {
            if (adjustment.value + adjustment.increment <= adjustment.lower ||
                adjustment.value + adjustment.increment >= adjustment.upper - adjustment.pageSize)
                adjustment.increment *= -1;

            adjustment.value += adjustment.increment;
        });
    }

    _updateFps() {
        let fps = this.get_frame_clock().get_fps();
        this._fps_label.label = vprintf("%.2f fps", [fps]);
    }

    get widgetType() {
        return this._widgetType;
    }

    set widgetType(widgetType) {
        if (widgetType === this._widgetType)
            return;

        this._widgetType = widgetType;

        if (this._tickId)
            this.remove_tick_callback(this._tickId);

        this.title = populateFunctions[widgetType].name;
        let { child, hscrollbarPolicy, hincrement, vincrement } = populateFunctions[widgetType]();

        this._scrolledwindow.child = child;
        this._scrolledwindow.hscrollbarPolicy = hscrollbarPolicy;
        this._scrolledwindow.hadjustment.increment = hincrement;
        this._scrolledwindow.vadjustment.increment = vincrement;

        this._tickId = this.add_tick_callback(() => {
            this._updateAdjustment();
            return GLib.SOURCE_CONTINUE;
        });
    }

    // Builder handler.
    iconscroll_prev_clicked_cb() {
        this.widgetType = this.widgetType == 0 ? populateFunctions.length - 1 : this.widgetType - 1;
    }

    // Builder handler.
    iconscroll_next_clicked_cb() {
        this.widgetType = this.widgetType == populateFunctions.length - 1 ? 0 : this.widgetType + 1;
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    new Window({ application }).present();
});

application.run([]);
