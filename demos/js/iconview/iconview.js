// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Icon View Basics
 *
 * The GtkIconView widget is used to display and manipulate icons.
 * It uses a GtkTreeModel for data storage, so the list store
 * example might be helpful.
 */

imports.gi.versions['Gtk'] = '4.0';
const { GdkPixbuf, Gio, GLib, GObject, Gtk } = imports.gi;

const Column = { FILE: 0, DISPLAY_NAME: 1, PIXBUF: 2, IS_DIRECTORY: 3 };
const FILE_PIXBUF = GdkPixbuf.Pixbuf.new_from_file('gnome-fs-regular.png');
const FOLDER_PIXBUF = GdkPixbuf.Pixbuf.new_from_file('gnome-fs-directory.png');

const DirectoryStore = GObject.registerClass({
    Properties: {
        'can-up': GObject.ParamSpec.boolean(
            'can-up', "Can up", "Whether the directy has parent",
            GObject.ParamFlags.READABLE, false
        ),
        'directory': GObject.ParamSpec.object(
            'directory', "Directory", "The directory this represents",
            GObject.ParamFlags.READWRITE, Gio.File.$gtype
        ),
    },
}, class DirectoryStore extends Gtk.ListStore {
    vfunc_constructed() {
        super.vfunc_constructed();

        this.set_column_types([Gio.File.$gtype, GObject.TYPE_STRING, GdkPixbuf.Pixbuf.$gtype, GObject.TYPE_BOOLEAN]);
        this.set_default_sort_func((model_, iterA, iterB) => {
            // We need this function because we want to sort folders before files.
            // Since there are no files starting with '.' (hidden files are ignored),
            // String.prototype.localeCompare should return the same order than GLib.utf8_collate.
            return (
                this.get_value(iterA, Column.IS_DIRECTORY) && !this.get_value(iterB, Column.IS_DIRECTORY) ? -1 :
                this.get_value(iterB, Column.IS_DIRECTORY) && !this.get_value(iterA, Column.IS_DIRECTORY) ? 1 :
                this.get_value(iterA, Column.DISPLAY_NAME).localeCompare(this.get_value(iterB, Column.DISPLAY_NAME))
            );
        });
        this.set_sort_column_id(Gtk.TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID, Gtk.SortType.ASCENDING);
    }

    get canUp() {
        return this.directory?.has_parent(null) || false;
    }

    get directory() {
        return this._directory || null;
    }

    set directory(directory) {
        if (this._directory == directory || (directory && this._directory?.equal(directory)))
            return;

        this._directory = directory;
        this.notify('can-up');

        // First clear the store.
        this.clear();

        // Then fill the store with the directory contents.
        let info, enumerator;
        try {
            enumerator = this.directory.enumerate_children('standard::name,standard::display-name,standard::is-hidden,standard::type', Gio.FileQueryInfoFlags.NONE, null);
            while ((info = enumerator.next_file(null))) {
                // We ignore hidden files.
                if (info.get_is_hidden())
                    continue;

                this.insert_with_values(-1, [Column.File, Column.DISPLAY_NAME, Column.PIXBUF, Column.IS_DIRECTORY], [
                    enumerator.get_child(info),
                    info.get_display_name(),
                    info.get_file_type() == Gio.FileType.DIRECTORY ? FOLDER_PIXBUF : FILE_PIXBUF,
                    info.get_file_type() == Gio.FileType.DIRECTORY,
                ]);
            }
        } catch(e) {
            logError(e);
        } finally {
            enumerator?.close(null);
        }
    }

    getFile(path) {
        return this.get_value(this.get_iter(path)[1], Column.FILE);
    }

    getIsDir(path) {
        return this.get_value(this.get_iter(path)[1], Column.IS_DIRECTORY);
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    // The model, initiated with the filesystem root.
    let store = new DirectoryStore({ directory: Gio.File.new_for_path('/') });

    // Some model controls.
    let upButton = new Gtk.Button({ label: "_Up", useUnderline: true });
    store.bind_property('can-up', upButton, 'sensitive', GObject.BindingFlags.SYNC_CREATE);
    upButton.connect('clicked', () => {
        store.directory = store.directory.get_parent();
    });
    let homeButton = new Gtk.Button({ label: "_Home", useUnderline: true });
    homeButton.connect('clicked', () => {
        store.directory = Gio.File.new_for_path(GLib.get_home_dir());
    });
    let toolbar = new Gtk.Box();
    toolbar.append(upButton);
    toolbar.append(homeButton);

    // The view.
    let iconView = new Gtk.IconView({
        model: store,
        selectionMode: Gtk.SelectionMode.MULTIPLE,
        textColumn: Column.DISPLAY_NAME,
        pixbufColumn: Column.PIXBUF,
    });
    iconView.connect('item-activated', (iconView, path) => {
        if (store.getIsDir(path))
            store.directory = store.getFile(path);
    });

    let scrolledWindow = new Gtk.ScrolledWindow({
        hasFrame: true, vexpand: true,
        hscrollbarPolicy: Gtk.PolicyType.AUTOMATIC,
        vscrollbarPolicy: Gtk.PolicyType.AUTOMATIC,
        child: iconView,
    });

    let vbox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
    vbox.append(toolbar);
    vbox.append(scrolledWindow);

    new Gtk.Window({
        application,
        title: "Icon View Basics",
        defaultWidth: 650, defaultHeight: 400,
        child: vbox,
        visible: true,
    });
});

application.run([]);
