// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Editing and Drag-and-Drop
 *
 * The GtkIconView widget supports Editing and Drag-and-Drop.
 * This example also demonstrates using the generic GtkCellLayout
 * interface to set up cell renderers in an icon view.
 *
 * FIXME: Buggy such as in GTK4 Demo.
 * Editing rendering is ugly and Drag-and-Drop is not working.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, GdkPixbuf, GObject, Gtk } = imports.gi;

const Column = { COLOR_STRING: 0 };
const COLOR_STRINGS = ['Red', 'Green', 'Blue', 'Yellow'];

let application = new Gtk.Application();

application.connect('activate', () => {
    let store = Gtk.ListStore.new([GObject.TYPE_STRING]);
    COLOR_STRINGS.forEach(colorString => {
        store.insert_with_values(-1, [Column.COLOR_STRING], [colorString]);
    });

    let renderer, iconView = new Gtk.IconView({
        model: store,
        selectionMode: Gtk.SelectionMode.SINGLE,
        itemOrientation: Gtk.Orientation.HORIZONTAL,
        columns: 2,
        reorderable: true,
    });

    iconView.pack_start(renderer = new Gtk.CellRendererPixbuf(), true);
    iconView.set_cell_data_func(renderer, (iconView_, renderer, store, iter) => {
        let colorString = store.get_value(iter, Column.COLOR_STRING);
        if (!colorString)
            return;

        let pixel = 0;
        let color = new Gdk.RGBA();
        if (color.parse(colorString))
            pixel = (
                color.red * 255 * 2 ** 24 +
                color.green * 255 * 2 ** 16 +
                color.blue * 255  * 2 ** 8 +
                color.alpha * 255
            );

        let pixbuf = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, true, 8, 24, 24);
        pixbuf.fill(pixel);

        renderer.pixbuf = pixbuf;
    });

    iconView.pack_start(renderer = new Gtk.CellRendererText({ editable: true }), true);
    iconView.add_attribute(renderer, 'text', Column.COLOR_STRING);
    renderer.connect('edited', (renderer, pathString, newText) => {
        let path = Gtk.TreePath.new_from_string(pathString);
        store.set_value(store.get_iter(path)[1], Column.COLOR_STRING, newText);
    });

    new Gtk.Window({
        application,
        title: "Editing and Drag-and-Drop",
        child: iconView,
        visible: true,
    });
});

application.run([]);
