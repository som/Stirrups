// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Info Bars
 *
 * Info bar widgets are used to report important messages to the user.
 */

imports.gi.versions['Gtk'] = '4.0';
const { GObject, Gtk } = imports.gi;
const _ = imports.gettext.domain('gtk40').gettext;
String.prototype.format = imports.format.format;

const INFO_BAR_LABEL = "This is an info bar with message type %s";

function onInfoBarResponse(infoBar, response) {
    if (response == Gtk.ResponseType.CLOSE) {
        infoBar.set_revealed(false);
        return;
    }

    let dialog = new Gtk.MessageDialog({
        transientFor: infoBar.root,
        modal: true, destroyWithParent: true,
        messageType: Gtk.MessageType.INFO,
        buttons: Gtk.ButtonsType.OK,
        text: "You clicked a button on an info bar",
        secondaryText: "Your response has id %d".format(response),
    });
    dialog.connect('response', () => dialog.destroy());
    dialog.show();
}

let application = new Gtk.Application();

application.connect('activate', () => {
    let vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        marginStart: 8, marginEnd: 8,
        marginTop: 8, marginBottom: 8,
    });

    let hbox = new Gtk.Box({
        halign: Gtk.Align.CENTER,
        marginStart: 8, marginEnd: 8,
        marginTop: 8, marginBottom: 8,
    });

    {
        let infoBar = new Gtk.InfoBar({ messageType: Gtk.MessageType.INFO });
        infoBar.add_child(new Gtk.Label({
            xalign: 0, wrap: true,
            label: INFO_BAR_LABEL.format("GTK_MESSAGE_INFO"),
        }));
        vbox.append(infoBar);

        let button = new Gtk.ToggleButton({ label: "Message" });
        button.bind_property('active', infoBar, 'revealed', GObject.BindingFlags.BIDIRECTIONAL | GObject.BindingFlags.SYNC_CREATE);
        hbox.append(button);
    }

    {
        let infoBar = new Gtk.InfoBar({ messageType: Gtk.MessageType.WARNING });
        infoBar.add_child(new Gtk.Label({
            xalign: 0, wrap: true,
            label: INFO_BAR_LABEL.format("GTK_MESSAGE_WARNING"),
        }));
        vbox.append(infoBar);

        let button = new Gtk.ToggleButton({ label: "Warning" });
        button.bind_property('active', infoBar, 'revealed', GObject.BindingFlags.BIDIRECTIONAL | GObject.BindingFlags.SYNC_CREATE);
        hbox.append(button);
    }

    {
        let infoBar = new Gtk.InfoBar({
            messageType: Gtk.MessageType.QUESTION,
            showCloseButton: true,
        });
        infoBar.add_child(new Gtk.Label({
            xalign: 0, wrap: true,
            label: INFO_BAR_LABEL.format("GTK_MESSAGE_QUESTION"),
        }));
        infoBar.add_button(_("_OK"), Gtk.ResponseType.OK);
        // "this function currently requires info_bar to be added to a widget hierarchy".
        let handler = infoBar.connect('notify::root', () => {
            infoBar.set_default_response(Gtk.ResponseType.OK);
            infoBar.disconnect(handler);
        });
        infoBar.connect('response', onInfoBarResponse);
        vbox.append(infoBar);

        let button = new Gtk.ToggleButton({ label: "Question" });
        button.bind_property('active', infoBar, 'revealed', GObject.BindingFlags.BIDIRECTIONAL | GObject.BindingFlags.SYNC_CREATE);
        hbox.append(button);
    }

    {
        let infoBar = new Gtk.InfoBar({ messageType: Gtk.MessageType.ERROR });
        infoBar.add_child(new Gtk.Label({
            xalign: 0, wrap: true,
            label: INFO_BAR_LABEL.format("GTK_MESSAGE_ERROR"),
        }));
        vbox.append(infoBar);

        let button = new Gtk.ToggleButton({ label: "Error" });
        button.bind_property('active', infoBar, 'revealed', GObject.BindingFlags.BIDIRECTIONAL | GObject.BindingFlags.SYNC_CREATE);
        hbox.append(button);
    }

    {
        let infoBar = new Gtk.InfoBar({ messageType: Gtk.MessageType.OTHER });
        infoBar.add_child(new Gtk.Label({
            xalign: 0, wrap: true,
            label: INFO_BAR_LABEL.format("GTK_MESSAGE_OTHER"),
        }));
        vbox.append(infoBar);

        let button = new Gtk.ToggleButton({ label: "Other" });
        button.bind_property('active', infoBar, 'revealed', GObject.BindingFlags.BIDIRECTIONAL | GObject.BindingFlags.SYNC_CREATE);
        hbox.append(button);
    }

    vbox.append(new Gtk.Frame({
        label: "An example of different info bars",
        marginTop: 8, marginBottom: 8,
        child: hbox,
    }));

    new Gtk.Window({
        application,
        title: "Info Bars",
        resizable: false,
        child: vbox,
    }).present();
});

application.run([]);
