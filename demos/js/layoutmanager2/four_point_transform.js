// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* exported perspective3D */

const { Graphene } = imports.gi;
const { singular_value_decomposition, singular_value_decomposition_solve } = imports.singular_value_decomposition;

/* Make a 4x4 matrix that maps
 * e1        -> p1
 * e2        -> p3
 * e3        -> p3
 * (1,1,1,0) -> p4
 */
const unitTo = function(p1, p2, p3, p4) {
    const A = new Float64Array(16);
    const B = new Float64Array(4);
    const U = new Float64Array(16);
    const S = new Float64Array(4);
    const V = new Float64Array(16);
    const x = new Float64Array(4);

    let v1 = new Graphene.Vec4().init_from_vec3(p1.to_vec3(), 1);
    let v2 = new Graphene.Vec4().init_from_vec3(p2.to_vec3(), 1);
    let v3 = new Graphene.Vec4().init_from_vec3(p3.to_vec3(), 1);
    let v4 = new Graphene.Vec4().init_from_vec3(p4.to_vec3(), 1);

    let p = new Graphene.Vec4().init(0, 0, 0, 1);
    let u = new Graphene.Matrix().init_from_vec4(v1, v2, v3, p);

    // Solve x * u = v4.

    for (let i = 0; i < 4; i++)
        for (let j = 0; j < 4; j++)
            A[j * 4 + i] = u.get_value(i, j);

    B[0] = v4.get_x();
    B[1] = v4.get_y();
    B[2] = v4.get_z();
    B[3] = v4.get_w();

    singular_value_decomposition(A, 4, 4, U, S, V);
    singular_value_decomposition_solve(U, S, V, 4, 4, B, x);

    let v = new Float32Array(16).fill(0);
    v[0] = x[0];
    v[5] = x[1];
    v[10] = x[2];
    v[15] = 1;

    let s = new Graphene.Matrix().init_from_float(v);

    return s.multiply(u);
};

/* Compute a 4x4 matrix m that maps
 * p1 -> q1
 * p2 -> q2
 * p3 -> q3
 * p4 -> q4
 *
 * This is not in general possible, because projective
 * transforms preserve coplanarity. But in the cases we
 * care about here, both sets of points are always coplanar.
 */
var perspective3D = function(p1, p2, p3, p4, q1, q2, q3, q4) {
    let a = unitTo(p1, p2, p3, p4);
    let b = unitTo(q1, q2, q3, q4);

    let [invertible, aInv] = a.inverse();
    if (!invertible)
        throw new Error(`${a} is not invertible`);

    return aInv.multiply(b);
};
