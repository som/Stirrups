// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* See Golub and Reinsch,
 * "Handbook for Automatic Computation vol II - Linear Algebra",
 * Springer, 1971
 */

/* exported singular_value_decomposition, singular_value_decomposition_solve */

const MAX_ITERATION_COUNT = 30;

/* Perform Householder reduction to bidiagonal form
 *
 * Input: Matrix A of size nrows x ncols
 *
 * Output: Matrices and vectors such that
 * A = U*Bidiag(diagonal, superdiagonal)*Vt
 *
 * All matrices are allocated by the caller
 *
 * Sizes:
 *  A, U: nrows x ncols
 *  diagonal, superdiagonal: ncols
 *  V: ncols x ncols
 */
// void
const householder_reduction = function(A, nrows, ncols, U, V, diagonal, superdiagonal) {
    if (nrows < 2 || ncols < 2)
        throw new Error("Incorrect params");

    let i, j, k, ip1; // int
    let s, s2, si, scale; // double
    let pu, pui, pv, pvi; // int
    let half_norm_squared; // double

    for (let i = 0; i < nrows * ncols; i++)
        U[i] = A[i];

    diagonal[0] = 0.0;
    s = 0.0;
    scale = 0.0;
    for (i = 0, pui = 0, ip1 = 1; i < ncols; pui += ncols, i++, ip1++) {
        superdiagonal[i] = scale * s;

        for (j = i, pu = pui, scale = 0.0; j < nrows; j++, pu += ncols)
            scale += Math.abs(U[pu + i]);

        if (scale > 0.0) {
            for (j = i, pu = pui, s2 = 0.0; j < nrows; j++, pu += ncols) {
                U[pu + i] /= scale;
                s2 += U[pu + i] * U[pu + i];
            }
            s = U[pui + i] < 0.0 ? Math.sqrt(s2) : -Math.sqrt(s2);
            half_norm_squared = U[pui + i] * s - s2;
            U[pui + i] -= s;

            for (j = ip1; j < ncols; j++) {
                for (k = i, si = 0.0, pu = pui; k < nrows; k++, pu += ncols)
                    si += U[pu + i] * U[pu + j];
                si /= half_norm_squared;
                for (k = i, pu = pui; k < nrows; k++, pu += ncols)
                    U[pu + j] += si * U[pu + i];
            }
        }
        for (j = i, pu = pui; j < nrows; j++, pu += ncols)
            U[pu + i] *= scale;
        diagonal[i] = s * scale;
        s = 0.0;
        scale = 0.0;
        if (i >= nrows || i == ncols - 1)
            continue;
        for (j = ip1; j < ncols; j++)
            scale += Math.abs(U[pui + j]);
        if (scale > 0.0) {
            for (j = ip1, s2 = 0.0; j < ncols; j++) {
                U[pui + j] /= scale;
                s2 += U[pui + j] * U[pui + j];
            }
            s = U[pui + ip1] < 0.0 ? Math.sqrt(s2) : -Math.sqrt(s2);
            half_norm_squared = U[pui + ip1] * s - s2;
            U[pui + ip1] -= s;
            for (k = ip1; k < ncols; k++)
                superdiagonal[k] = U[pui + k] / half_norm_squared;
            if (i < (nrows - 1)) {
                for (j = ip1, pu = pui + ncols; j < nrows; j++, pu += ncols) {
                    for (k = ip1, si = 0.0; k < ncols; k++)
                        si += U[pui + k] * U[pu + k];
                    for (k = ip1; k < ncols; k++)
                        U[pu + k] += si * superdiagonal[k];
                }
            }
            for (k = ip1; k < ncols; k++)
                U[pui + k] *= scale;
        }
    }

    pui = ncols * (ncols - 2);
    pvi = ncols * (ncols - 1);
    V[pvi + ncols - 1] = 1.0;
    s = superdiagonal[ncols - 1];
    pvi -= ncols;
    for (i = ncols - 2, ip1 = ncols - 1; i >= 0; i--, pui -= ncols, pvi -= ncols, ip1--) {
        if (s != 0.0) {
            pv = pvi + ncols;
            for (j = ip1; j < ncols; j++, pv += ncols)
                V[pv + i] = (U[pui + j] / U[pui + ip1]) / s;
            for (j = ip1; j < ncols; j++) {
                si = 0.0;
                for (k = ip1, pv = pvi + ncols; k < ncols; k++, pv += ncols)
                    si += U[pui + k] * V[pv + j];
                for (k = ip1, pv = pvi + ncols; k < ncols; k++, pv += ncols)
                    V[pv + j] += si * V[pv + i];
            }
        }
        pv = pvi + ncols;
        for (j = ip1; j < ncols; j++, pv += ncols) {
            V[pvi + j] = 0.0;
            V[pv + i] = 0.0;
        }
        V[pvi + i] = 1.0;
        s = superdiagonal[i];
    }

    pui = ncols * (ncols - 1);
    for (i = ncols - 1, ip1 = ncols; i >= 0; ip1 = i, i--, pui -= ncols) {
        s = diagonal[i];
        for (j = ip1; j < ncols; j++)
            U[pui + j] = 0.0;
        if (s != 0.0) {
            for (j = ip1; j < ncols; j++) {
                si = 0.0;
                pu = pui + ncols;
                for (k = ip1; k < nrows; k++, pu += ncols)
                    si += U[pu + i] * U[pu + j];
                si = (si / U[pui + i]) / s;
                for (k = i, pu = pui; k < nrows; k++, pu += ncols)
                    U[pu + j] += si * U[pu + i];
            }
            for (j = i, pu = pui; j < nrows; j++, pu += ncols)
                U[pu + i] /= s;
        } else {
            for (j = i, pu = pui; j < nrows; j++, pu += ncols)
                U[pu + i] = 0.0;
        }
        U[pui + i] += 1.0;
    }
};

/* Perform Givens reduction
 *
 * Input: Matrices such that
 * A = U*Bidiag(diagonal,superdiagonal)*Vt
 *
 * Output: The same, with superdiagonal = 0
 *
 * All matrices are allocated by the caller
 *
 * Sizes:
 *  U: nrows x ncols
 *  diagonal, superdiagonal: ncols
 *  V: ncols x ncols
 */
// int
const givens_reduction = function(nrows, ncols, U, V, diagonal, superdiagonal) {
    if (nrows < 2 || ncols < 2)
        throw new Error("Incorrect params");

    let c, s; // double
    let f, g, h; // double
    let x, y, z; // double
    let pu, pv; // int
    let i, j, k, m; // int
    let rotation_test; // int
    let iteration_count; // int

    for (i = 0, x = 0.0; i < ncols; i++) {
        y = Math.abs(diagonal[i]) + Math.abs(superdiagonal[i]);
        if (x < y)
            x = y;
    }
    let epsilon = x * Number.EPSILON;
    for (k = ncols - 1; k >= 0; k--) {
        iteration_count = 0;
        while (true) {
            rotation_test = 1;
            for (m = k; m >= 0; m--) {
                if (Math.abs(superdiagonal[m]) <= epsilon) {
                    rotation_test = 0;
                    break;
                }
                if (Math.abs(diagonal[m - 1]) <= epsilon)
                    break;
            }
            if (rotation_test) {
                c = 0.0;
                s = 1.0;
                for (i = m; i <= k; i++) {
                    f = s * superdiagonal[i];
                    superdiagonal[i] *= c;
                    if (Math.abs(f) <= epsilon)
                        break;
                    g = diagonal[i];
                    h = Math.sqrt(f * f + g * g);
                    diagonal[i] = h;
                    c = g / h;
                    s = -f / h;
                    for (j = 0, pu = 0; j < nrows; j++, pu += ncols) {
                        y = U[pu + m - 1];
                        z = U[pu + i];
                        U[pu + m - 1] = y * c + z * s;
                        U[pu + i] = -y * s + z * c;
                    }
                }
            }
            z = diagonal[k];
            if (m == k) {
                if (z < 0.0) {
                    diagonal[k] = -z;
                    for (j = 0, pv = 0; j < ncols; j++, pv += ncols)
                        V[pv + k] = -V[pv + k];
                }
                break;
            } else {
                if (iteration_count >= MAX_ITERATION_COUNT)
                    return -1;
                iteration_count++;
                x = diagonal[m];
                y = diagonal[k - 1];
                g = superdiagonal[k - 1];
                h = superdiagonal[k];
                f = ((y - z) * (y + z) + (g - h) * (g + h)) / (2.0 * h * y);
                g = Math.sqrt(f * f + 1.0);
                if (f < 0.0)
                    g = -g;
                f = ((x - z) * (x + z) + h * (y / (f + g) - h)) / x;
                c = 1.0;
                s = 1.0;
                for (i = m + 1; i <= k; i++) {
                    g = superdiagonal[i];
                    y = diagonal[i];
                    h = s * g;
                    g *= c;
                    z = Math.sqrt(f * f + h * h);
                    superdiagonal[i - 1] = z;
                    c = f / z;
                    s = h / z;
                    f =  x * c + g * s;
                    g = -x * s + g * c;
                    h = y * s;
                    y *= c;
                    for (j = 0, pv = 0; j < ncols; j++, pv += ncols) {
                        x = V[pv + i - 1];
                        z = V[pv + i];
                        V[pv + i - 1] = x * c + z * s;
                        V[pv + i] = -x * s + z * c;
                    }
                    z = Math.sqrt(f * f + h * h);
                    diagonal[i - 1] = z;
                    if (z != 0.0) {
                        c = f / z;
                        s = h / z;
                    }
                    f = c * g + s * y;
                    x = -s * g + c * y;
                    for (j = 0, pu = 0; j < nrows; j++, pu += ncols) {
                        y = U[pu + i - 1];
                        z = U[pu + i];
                        U[pu + i - 1] = c * y + s * z;
                        U[pu + i] = -s * y + c * z;
                    }
                }
                superdiagonal[m] = 0.0;
                superdiagonal[k] = f;
                diagonal[k] = x;
            }
        }
    }
    return 0;
};

/* Given a singular value decomposition
 * of an nrows x ncols matrix A = U*Diag(S)*Vt,
 * sort the values of S by decreasing value,
 * permuting V to match.
 */
// void
const sort_singular_values = function(nrows, ncols, S, U, V) {
    if (nrows < 2 || ncols < 2)
        throw new Error("Incorrect params");

    let i, j, max_index; // int
    let p1, p2; // int

    for (i = 0; i < ncols - 1; i++) {
        max_index = i;
        for (j = i + 1; j < ncols; j++)
            if (S[j] > S[max_index])
                max_index = j;
        if (max_index == i)
            continue;
        [S[i], S[max_index]] = [S[max_index], S[i]];
        p1 = max_index;
        p2 = i;
        for (j = 0; j < nrows; j++, p1 += ncols, p2 += ncols)
            [U[p1], U[p2]] = [U[p2], U[p1]];
        p1 = max_index;
        p2 = i;
        for (j = 0; j < ncols; j++, p1 += ncols, p2 += ncols)
            [V[p1], V[p2]] = [V[p2], V[p1]];
    }
};

/* Compute a singular value decomposition of A,
 * A = U*Diag(S)*Vt
 *
 * All matrices are allocated by the caller
 *
 * Sizes:
 *  A, U: nrows x ncols
 *  S: ncols
 *  V: ncols x ncols
 */
// int
var singular_value_decomposition = function(A, nrows, ncols, U, S, V) {
    if (nrows < ncols)
        return -1;

    const superdiagonal = new Array(ncols);

    householder_reduction(A, nrows, ncols, U, V, S, superdiagonal);

    if (givens_reduction(nrows, ncols, U, V, S, superdiagonal) < 0)
        return -1;

    sort_singular_values(nrows, ncols, S, U, V);

    return 0;
};

/*
 * Given a singular value decomposition of A = U*Diag(S)*Vt,
 * compute the best approximation x to A*x = B.
 *
 * All matrices are allocated by the caller
 *
 * Sizes:
 *  U: nrows x ncols
 *  S: ncols
 *  V: ncols x ncols
 *  B, x: ncols
 */
// void
var singular_value_decomposition_solve = function(U, S, V, nrows, ncols, B, x) {
    if (nrows < 2 || ncols < 2)
        throw new Error("Incorrect params");

    let i, j, k; // int
    let pu, pv; // int
    let d; // double

    let tolerance = Number.EPSILON * S[0] * ncols;

    for (i = 0, pv = 0; i < ncols; i++, pv += ncols) {
        x[i] = 0.0;
        for (j = 0; j < ncols; j++) {
            if (S[j] > tolerance) {
                for (k = 0, d = 0.0, pu = 0; k < nrows; k++, pu += ncols)
                    d += U[pu + j] * B[k];
                x[i] += d * V[pv + j] / S[j];
            }
        }
    }
};
