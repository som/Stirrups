// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Links
 *
 * GtkLabel can show hyperlinks. The default action is to call
 * gtk_show_uri() on their URI, but it is possible to override
 * this with a custom handler.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gtk, Pango } = imports.gi;

let application = new Gtk.Application();

application.connect('activate', () => {
    let label = new Gtk.Label({
        marginStart: 20, marginEnd: 20,
        marginTop: 20, marginBottom: 20,
        maxWidthChars: 40,
        wrap: true, wrapMode: Pango.WrapMode.WORD,
    });

    label.set_markup(
        'Some <a href="https://en.wikipedia.org/wiki/Text"'
        + 'title="plain text">text</a> may be marked up '
        + 'as hyperlinks, which can be clicked '
        + 'or activated via <a href="keynav">keynav</a> '
        + 'and they work fine with other markup, like when '
        + 'searching on <a href="https://www.example.com/">'
        + '<span color="#0266C8">G</span><span color="#F90101">o</span>'
        + '<span color="#F2B50F">o</span><span color="#0266C8">g</span>'
        + '<span color="#00933B">l</span><span color="#F90101">e</span>'
        + '</a>.'
    );

    label.connect('activate-link', (label, uri) => {
        if (uri != 'keynav')
            return false;

        let dialog = new Gtk.MessageDialog({
            transientFor: label.root,
            modal: true, destroyWithParent: true,
            messageType: Gtk.MessageType.INFO,
            buttons: Gtk.ButtonsType.OK,
            text: "Keyboard navigation",
            secondaryText: (
                "The term <i>keynav</i> is a shorthand for "
                + "keyboard navigation and refers to the process of using "
                + "a program (exclusively) via keyboard input."
            ),
            secondaryUseMarkup: true,
        });
        dialog.connect('response', () => dialog.destroy());
        dialog.show();

        return true;
    });

    new Gtk.Window({
        application,
        title: "Links",
        resizable: false,
        child: label,
    }).present();
});

application.run([]);
