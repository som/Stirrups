// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Controls
 *
 * GtkListBox is well-suited for creating “button strips” — lists of
 * controls for use in preference dialogs or settings panels. To create
 * this style of list, use the .rich-list style class.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');

// Alternative to the original GTK demo 'row_activated' builder
// handler and the related builder scope definition.
// Use the Actionable interface of list box rows to bind its
// activation with a child widget and a callback.
const setRowAction = function(widget, callback = () => widget.activate()) {
    let action = new Gio.SimpleAction({ name: 'activate' });
    action.connect('activate', callback.bind(null, widget));

    let actionGroup = new Gio.SimpleActionGroup();
    actionGroup.add_action(action);

    let row = widget.get_ancestor(Gtk.ListBoxRow.$gtype);
    row.insert_action_group('row', actionGroup);
    row.set_action_name('row.activate');
};

let application = new Gtk.Application();

application.connect('activate', () => {
    let builder = Gtk.Builder.new_from_file(directory.get_child('listbox_controls.ui').get_path());

    setRowAction(builder.get_object('check'));
    setRowAction(builder.get_object('image'), widget => widget.set_opacity(1 - widget.opacity));
    setRowAction(builder.get_object('switch'));

    let window = builder.get_object('window');
    window.set_application(application);
    window.present();
});

application.run([]);
