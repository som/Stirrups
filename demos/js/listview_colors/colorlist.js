// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* exported Color, ColorList */

const { Gdk, Gio, GLib, GObject, Graphene, Gtk } = imports.gi;
const ByteArray = imports.byteArray;

let colorNamesContents;
try {
    let output = ByteArray.toString(GLib.spawn_command_line_sync('whereis -b gtk4-demo')[1]);
    let demoPath = output.split(' ')[1];

    // FIXME: Gio.Resource.load('/usr/bin/gtk4-demo') ->
    // "Gio.ResourceError: /usr/bin/gtk4-demo: invalid gvdb header".
    colorNamesContents = ByteArray.toString(
        GLib.spawn_command_line_sync(`gresource extract ${demoPath} /listview_colors/color.names.txt`)[1]);
} catch(e) {
    logError(e);
}

var Color = GObject.registerClass({
    Implements: [Gdk.Paintable],
    Properties: {
        'name': GObject.ParamSpec.string(
            'name', "Name", "The color name",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, ''
        ),
        'rgba': GObject.ParamSpec.boxed(
            'rgba', "RGBA", "The color RGBA",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, Gdk.RGBA.$gtype
        ),
        'red': GObject.ParamSpec.uint(
            'red', "Red", "The red component",
            GObject.ParamFlags.READABLE, 0, 255, 0
        ),
        'green': GObject.ParamSpec.uint(
            'green', "Green", "The green component",
            GObject.ParamFlags.READABLE, 0, 255, 0
        ),
        'blue': GObject.ParamSpec.uint(
            'blue', "Blue", "The blue component",
            GObject.ParamFlags.READABLE, 0, 255, 0
        ),
        'hue': GObject.ParamSpec.uint(
            'hue', "Hue", "The hue component",
            GObject.ParamFlags.READABLE, 0, 360, 0
        ),
        'saturation': GObject.ParamSpec.uint(
            'saturation', "Saturation", "The saturation component",
            GObject.ParamFlags.READABLE, 0, 100, 0
        ),
        'value': GObject.ParamSpec.uint(
            'value', "Value", "The value component",
            GObject.ParamFlags.READABLE, 0, 100, 0
        ),
    },
}, class Color extends GObject.Object {
    static newAverage(colors) {
        let { r, g, b } = colors.reduce((accumulator, color) => {
            accumulator.r += color.rgba.red;
            accumulator.g += color.rgba.green;
            accumulator.b += color.rgba.blue;
            return accumulator;
        }, { r: 0, g: 0, b: 0 });

        return new this({
            rgba: new Gdk.RGBA({
                red: r / colors.length,
                green: g / colors.length,
                blue: b / colors.length,
                alpha: 1,
            }),
        });
    }

    vfunc_get_intrinsic_width() {
        return 32;
    }

    vfunc_get_intrinsic_height() {
        return 32;
    }

    vfunc_snapshot(snapshot, width, height) {
        snapshot.append_color(this.rgba, new Graphene.Rect({ size: { width, height } }));
    }

    get red() {
        return Math.round(this.rgba.red * 255);
    }

    get green() {
        return Math.round(this.rgba.green * 255);
    }

    get blue() {
        return Math.round(this.rgba.blue * 255);
    }

    get _hsv() {
        if (!this.__hsv) {
            let [h, s, v] = Gtk.rgb_to_hsv(this.rgba.red, this.rgba.green, this.rgba.blue);
            this.__hsv = {
                hue: Math.round(h * 360),
                saturation: Math.round(s * 100),
                value: Math.round(v * 100),
            };
        }

        return this.__hsv;
    }

    get hue() {
        return this._hsv.hue;
    }

    get saturation() {
        return this._hsv.saturation;
    }

    get value() {
        return this._hsv.value;
    }
});

const N_COLORS = 256 * 256 * 256;
const MAP = [
    0xFF0000, 0x00FF00, 0x0000FF,
    0x7F0000, 0x007F00, 0x00007F,
    0x3F0000, 0x003F00, 0x00003F,
    0x1F0000, 0x001F00, 0x00001F,
    0x0F0000, 0x000F00, 0x00000F,
    0x070000, 0x000700, 0x000007,
    0x030000, 0x000300, 0x000003,
    0x010000, 0x000100, 0x000001,
];

var ColorList = GObject.registerClass({
    Implements: [Gio.ListModel],
    Properties: {
        'size': GObject.ParamSpec.uint(
            'size', "Size", "The list size",
            GObject.ParamFlags.READWRITE, 0, N_COLORS, 0
        ),
    }
}, class ColorList extends GObject.Object {
    static _positionToColor(position) {
        let result = 0;

        for (let i = 0; i < MAP.length; i++)
            if (position & (1 << i))
                result ^= MAP[i];

        return result;
    }

    _init(params) {
        this._colors = new Array(N_COLORS);

        super._init(params);

        if (!colorNamesContents)
            return;

        colorNamesContents.split('\n').forEach(line => {
            if (line.startsWith('#'))
                return;

            let [, name,, red, green, blue] = line.split(' ');
            let position = ((red & 0xFF) << 16) | ((green & 0xFF) << 8) | blue;

            if (!this._colors[position])
                this._colors[position] = new Color({
                    name,
                    rgba: new Gdk.RGBA({
                        red: red / 255,
                        green: green / 255,
                        blue: blue / 255,
                        alpha: 1
                    }),
                });
        });
    }

    vfunc_get_item_type() {
        return Color.$gtype;
    }

    vfunc_get_n_items() {
        return this.size;
    }

    vfunc_get_item(position) {
        if (position >= this.size)
            return null;

        position = this.constructor._positionToColor(position);

        if (!this._colors[position]) {
            this._colors[position] = new Color({
                rgba: new Gdk.RGBA({
                    red: ((position >> 16) & 0xFF) / 255,
                    green: ((position >> 8) & 0xFF) / 255,
                    blue: (position & 0xFF) / 255,
                    alpha: 1,
                }),
            });
        }

        return this._colors[position];
    }

    get size() {
        return this._size ?? 0;
    }

    set size(size) {
        if (this.size == size)
            return;

        let oldSize = this.size;
        this._size = size;
        this.notify('size');

        if (size > oldSize)
            this.items_changed(oldSize, 0, size - oldSize);
        else
            this.items_changed(size, oldSize - size, 0);
    }
});
