// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* exported HeaderBar */

const { Gio, GLib, GObject, Gtk, Pango } = imports.gi;

const DropDownFactory = GObject.registerClass(class DropDownFactory extends Gtk.SignalListItemFactory {
    static getLimit(position) {
        return 1 << (3 * (position + 1));
    }

    on_setup(listitem) {
        listitem.child = new Gtk.Label({
            xalign: 1,
            attributes: new Pango.AttrList(),
        });
        listitem.child.attributes.insert(Pango.attr_font_features_new('tnum'));
    }

    on_bind(listitem) {
        let limit = DropDownFactory.getLimit(listitem.position);
        listitem.child.label = String(limit);
    }
});

var HeaderBar = GObject.registerClass(class HeaderBar extends Gtk.HeaderBar {
    _init(selectionInfoRevealer, gridView, sorters, factories) {
        let selection = gridView.model;
        let sortModel = selection.model;
        let colorList = sortModel.model;
        this._limit = 0;

        super._init();

        let selectionInfoToggle = new Gtk.ToggleButton({
            iconName: 'emblem-important-symbolic',
            tooltipText: "Show selection info",
        });
        selectionInfoToggle.bind_property('active', selectionInfoRevealer, 'reveal-child', GObject.BindingFlags.DEFAULT);

        let refillButton = new Gtk.Button({
            label: "_Refill",
            useUnderline: true,
        });
        refillButton.connect('clicked', refillButton => {
            colorList.size = 0;

            refillButton.add_tick_callback(() => {
                colorList.size = Math.min(this._limit, colorList.size + Math.max(1, this._limit / 4096));

                return colorList.size >= this._limit ? GLib.SOURCE_REMOVE : GLib.SOURCE_CONTINUE;
            });
        });

        let sizeLabel = new Gtk.Label({
            label: "0 /",
            widthChars: String(4096).length + 2, xalign: 1,
            attributes: new Pango.AttrList(),
        });
        sizeLabel.attributes.insert(Pango.attr_font_features_new('tnum'));
        selection.connect('items-changed', () => {
            sizeLabel.label = `${selection.get_n_items()} /`;
        });
        let sizeDropDown = new Gtk.DropDown({
            model: Gtk.StringList.new(
                ["8", "64", "512", "4096", "32768", "262144", "2097152", "16777216"]
            ),
            factory: new DropDownFactory(),
        });
        sizeDropDown.connect('notify::selected', sizeDropDown => {
            let limit = DropDownFactory.getLimit(sizeDropDown.selected);
            sizeLabel.widthChars = String(limit).length + 2;
            if (this._limit == colorList.size)
                colorList.size = limit;
            this._limit = limit;
        });
        sizeDropDown.selected = 3; // 4096

        let sorterLabel = new Gtk.Label({ label: "Sort by:" });
        let sorterDropDown = new Gtk.DropDown({
            model: new Gio.ListStore({ itemType: Gtk.Sorter.$gtype }),
            expression: Gtk.ClosureExpression.new(GObject.TYPE_STRING, item => item.title, null),
        });
        sorterDropDown.model.append(...Object.entries(sorters).map(
            ([title, sorter]) => Object.assign(sorter, { title })
        ));
        sorterDropDown.bind_property('selected-item', sortModel, 'sorter', GObject.BindingFlags.SYNC_CREATE);

        let factoryLabel = new Gtk.Label({ label: "Show:" });
        let factoryDropDown = new Gtk.DropDown({
            model: new Gio.ListStore({ itemType: Gtk.ListItemFactory.$gtype }),
            expression: Gtk.ClosureExpression.new(GObject.TYPE_STRING, item => item.title, null),
        });
        factoryDropDown.model.append(...Object.entries(factories).map(
            ([title, factory]) => Object.assign(factory, { title })
        ));
        factoryDropDown.bind_property('selected-item', gridView, 'factory', GObject.BindingFlags.SYNC_CREATE);

        this.pack_start(selectionInfoToggle);
        this.pack_start(refillButton);
        this.pack_start(sizeLabel);
        this.pack_start(sizeDropDown);

        let sorterBox = new Gtk.Box({ spacing: 10 });
        sorterBox.append(sorterLabel, sorterDropDown);
        this.pack_end(sorterBox);

        let factoryBox = new Gtk.Box({ spacing: 10 });
        factoryBox.append(factoryLabel, factoryDropDown);
        this.pack_end(factoryBox);
    }
});
