// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Colors
 *
 * This demo displays a grid of colors.
 *
 * It is using a GtkGridView, and shows how to display
 * and sort the data in various ways. The controls for
 * this are implemented using GtkDropDown.
 *
 * The dataset used here has up to 16 777 216 items.
 *
 * Note that this demo also functions as a performance
 * test for some of the list model machinery, and the
 * biggest sizes here can lock up the application for
 * extended times when used with sorting.
 *
 * The color names are taken from the GTK4 Demos resources,
 * if installed.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gio, GObject, Gtk } = imports.gi;
const { Color, ColorList } = imports.colorlist;
const { HeaderBar } = imports.headerbar;
const directory = Gio.File.new_for_path('.');
const cssFile = directory.get_child('listview_colors.css');

// Make the "append" methods more convenient.
[Gtk.Box, Gtk.MultiSorter, Gio.ListStore].forEach(Klass => {
    Klass.prototype.appendOld = Klass.prototype.append;
    Klass.prototype.append = function(...elements) {
        elements.forEach(element => this.appendOld(element));
    };
});

// Make SelectionFilterModel instances iterable.
// TODO: Report bug to GTK.
// selectionFilterModel.get_n_items() returns non-null items for i >= selectionFilterModel.get_n_items().
Gtk.SelectionFilterModel.prototype[Symbol.iterator] = function*() {
    for (let i = 0; i < this.get_n_items(); i++)
        yield this.get_item(i);
};

// GJS automatically links the "on_signalname" methods, using GObject.signal_override_class_closure.
const SimpleFactory = GObject.registerClass({
    Properties: {
        'picture-size': GObject.ParamSpec.uint(
            'picture-size', "Picture size", "The picture size",
            GObject.ParamFlags.READWRITE, 0, 256, 32
        ),
    }
}, class SimpleFactory extends Gtk.SignalListItemFactory {
    on_setup(listItem) {
        listItem.child = new Gtk.Picture({ widthRequest: this.pictureSize, heightRequest: this.pictureSize });
        Gtk.PropertyExpression.new(Gtk.ListItem.$gtype, null, 'item')
            .bind(listItem.child, 'paintable', listItem);
    }
});

const ComplexFactory = GObject.registerClass({
}, class ComplexFactory extends Gtk.SignalListItemFactory {
    on_setup(listItem) {
        let colorExpression = Gtk.PropertyExpression.new(
            Gtk.ListItem.$gtype,
            // ❓️ Alternative : Gtk.ObjectExpression.new(listItem)
            Gtk.ConstantExpression.new_for_value(listItem),
            'item'
        );

        let picture = new Gtk.Picture();
        colorExpression.bind(picture, 'paintable', null);

        let nameLabel = new Gtk.Label();
        Gtk.PropertyExpression.new(Color.$gtype, colorExpression, 'name')
            .bind(nameLabel, 'label', null);

        let rgbLabel = new Gtk.Label({ useMarkup: true });
        Gtk.ClosureExpression.new(GObject.TYPE_STRING, (this_, color) => {
            if (!color)
                return null;
            return `<b>R:</b> ${color.red} <b>G:</b> ${color.green} <b>B:</b> ${color.blue}`;
        }, [colorExpression]).bind(rgbLabel, 'label', null);

        let hsvLabel = new Gtk.Label({ useMarkup: true });
        Gtk.ClosureExpression.new(GObject.TYPE_STRING, (this_, color) => {
            if (!color)
                return null;
            return `<b>H:</b> ${color.hue} <b>S:</b> ${color.saturation} <b>V:</b> ${color.value}`;
        }, [colorExpression]).bind(hsvLabel, 'label', null);

        listItem.child = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        listItem.child.append(nameLabel, picture, rgbLabel, hsvLabel);
    }
});

const factories = {
    "Colors": new SimpleFactory(),
    "Everything": new ComplexFactory(),
};

const sorters = {
    // An empty multisorter doesn't do any sorting.
    "Unsorted": new Gtk.MultiSorter(),
    "Name": new Gtk.StringSorter({ expression: Gtk.PropertyExpression.new(Color.$gtype, null, 'name') }),
    "Red": new Gtk.NumericSorter({ expression: Gtk.PropertyExpression.new(Color.$gtype, null, 'red') }),
    "Green": new Gtk.NumericSorter({ expression: Gtk.PropertyExpression.new(Color.$gtype, null, 'green') }),
    "Blue": new Gtk.NumericSorter({ expression: Gtk.PropertyExpression.new(Color.$gtype, null, 'blue') }),
    "RGB": new Gtk.MultiSorter(),
    "Hue": new Gtk.NumericSorter({ expression: Gtk.PropertyExpression.new(Color.$gtype, null, 'hue') }),
    "Saturation": new Gtk.NumericSorter({ expression: Gtk.PropertyExpression.new(Color.$gtype, null, 'saturation') }),
    "Value": new Gtk.NumericSorter({ expression: Gtk.PropertyExpression.new(Color.$gtype, null, 'value') }),
    "HSV": new Gtk.MultiSorter(),
};
sorters["RGB"].append(sorters["Red"], sorters["Green"], sorters["Blue"]);
sorters["HSV"].append(sorters["Hue"], sorters["Saturation"], sorters["Value"]);

let sortModel = new Gtk.SortListModel({ model: new ColorList(), incremental: true });
let selection = new Gtk.MultiSelection({ model: sortModel });
let selectionFilter = new Gtk.SelectionFilterModel({ model: selection });
let noSelection = new Gtk.NoSelection({ model: selectionFilter });

let application = new Gtk.Application();

application.connect('activate', () => {
    let selectionInfoRevealer = new Gtk.Revealer({
        child: new Gtk.Grid({
            marginStart: 10, marginEnd: 10,
            marginTop: 10, marginBottom: 10,
            rowSpacing: 10, columnSpacing: 10,
        }),
    });

    selectionInfoRevealer.child.attach(new Gtk.Label({
        label: "Selection", hexpand: true, cssClasses: ['title-3'],
    }), 0, 0, 5, 1);

    selectionInfoRevealer.child.attach(new Gtk.Label({
        label: "Size:",
    }), 0, 2, 1, 1);

    selectionInfoRevealer.child.attach(new Gtk.Label({
        label: "0",
    }), 1, 2, 1, 1);

    selectionInfoRevealer.child.attach(new Gtk.Label({
        label: "Average:",
    }), 2, 2, 1, 1);

    selectionInfoRevealer.child.attach(new Gtk.Picture({
        widthRequest: 32, heightRequest: 32,
    }), 3, 2, 1, 1);

    selectionInfoRevealer.child.attach(new Gtk.Label({
        label: "", hexpand: true,
    }), 4, 2, 1, 1);

    selectionInfoRevealer.child.attach(new Gtk.ScrolledWindow({
        hexpand: true, hscrollbarPolicy: Gtk.PolicyType.NEVER,
        child: new Gtk.GridView({
            model: noSelection,
            factory: new SimpleFactory({ pictureSize: 8 }),
            cssClasses: ['compact', 'view'],
            maxColumns: 200,
        }),
    }), 0, 1, 5, 1);

    selectionFilter.connect('items-changed', selectionFilter => {
        selectionInfoRevealer.child.get_child_at(1, 2).label = String(selectionFilter.get_n_items());
        selectionInfoRevealer.child.get_child_at(3, 2).paintable = Color.newAverage([...selectionFilter]);
    });

    // The main grid view.
    let gridView = new Gtk.GridView({
        model: selection,
        maxColumns: 24,
        enableRubberband: true,
        hscrollPolicy: Gtk.ScrollablePolicy.NATURAL,
        vscrollPolicy: Gtk.ScrollablePolicy.NATURAL,
    });

    let box = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
    });
    box.append(selectionInfoRevealer, new Gtk.ScrolledWindow({
        hexpand: true, vexpand: true,
        child: gridView,
    }));

    let progressBar = new Gtk.ProgressBar({
        hexpand: true, valign: Gtk.Align.START,
    });
    sortModel.connect('notify::pending', sortModel => {
        progressBar.visible = Boolean(sortModel.pending);
        progressBar.fraction = (sortModel.get_n_items() - sortModel.pending) / Math.max(1, sortModel.get_n_items());
    });

    let overlay = new Gtk.Overlay({ child: box });
    overlay.add_overlay(progressBar);

    let window = new Gtk.Window({
        application, title: "Colors",
        defaultWidth: 600, defaultHeight: 400,
        child: overlay,
    });

    let headerBar = new HeaderBar(selectionInfoRevealer, gridView, sorters, factories);
    window.set_titlebar(headerBar);

    let cssProvider = new Gtk.CssProvider();
    cssProvider.load_from_file(cssFile);
    Gtk.StyleContext.add_provider_for_display(window.display, cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

    window.present();
});

application.run([]);
