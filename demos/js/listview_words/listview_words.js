// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Words
 *
 * This demo shows filtering a long list - of words.
 *
 * You should open a file such as `/usr/share/dict/words`.
 */

imports.gi.versions['Gtk'] = '4.0';
const { GLib, Gio, Gtk } = imports.gi;
const _ = imports.gettext.domain('gtk40').gettext;
const _N = imports.gettext.domain('gtk40').ngettext;
const ByteArray = imports.byteArray;
String.prototype.format = imports.format.format;

const FACTORY =
`<?xml version='1.0' encoding='UTF-8'?>
<interface>
  <template class='GtkListItem'>
    <property name='child'>
      <object class='GtkLabel'>
        <property name='ellipsize'>end</property>
        <property name='xalign'>0</property>
        <binding name='label'>
          <lookup name='string' type='GtkStringObject'>
            <lookup name='item'>GtkListItem</lookup>
          </lookup>
        </binding>
      </object>
    </property>
  </template>
</interface>`;

const WORDS = [
    "lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipisci", "elit",
    "sed", "eiusmod", "tempor", "incidunt", "labore", "et", "dolore", "magna",
    "aliqua", "ut", "enim", "ad", "minim", "veniam", "quis", "nostrud",
    "exercitation", "ullamco", "laboris", "nisi", "ut", "aliquid", "ex", "ea",
    "commodi", "consequat",
];

// TODO: Replace GioDataInputStream with GioBufferedInputStream for more celerity.
function readLines(dataStream, stringList) {
    dataStream.read_line_async(GLib.PRIORITY_HIGH_IDLE, null, (source, result) => {
        try {
            let byteArray = source.read_line_finish(result)[0];

            if (byteArray) {
                stringList.append(ByteArray.toString(byteArray));
                readLines(dataStream, stringList);
            }
        } catch(e) {
            log(`Could not read data: ${e.message}`);
        }
    });
}

function loadFile(file, stringList) {
    stringList.splice(0, stringList.get_n_items(), null);

    file.read_async(GLib.PRIORITY_HIGH_IDLE, null, (source, result) => {
        try {
            let baseStream = source.read_finish(result);
            readLines(new Gio.DataInputStream({ baseStream }), stringList);
        } catch(e) {
            log(`Could not open file: ${e.message}`);
        }
    });
}

let factory = new Gtk.BuilderListItemFactory({
    bytes: GLib.Bytes.new(FACTORY),
});

let filterModel = new Gtk.FilterListModel({
    model: Gtk.StringList.new(WORDS),
    filter: new Gtk.StringFilter({
        expression: Gtk.PropertyExpression.new(Gtk.StringObject.$gtype, null, 'string'),
    }),
    incremental: true,
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let listView = new Gtk.ListView({
        model: new Gtk.NoSelection({ model: filterModel }),
        factory,
    });

    let progressBar = new Gtk.ProgressBar({
        valign: Gtk.Align.START, hexpand: true,
    });

    let overlay = new Gtk.Overlay({
        child: new Gtk.ScrolledWindow({
            vexpand: true,
            child: listView,
        }),
    });
    overlay.add_overlay(progressBar);

    let searchEntry = new Gtk.SearchEntry();
    searchEntry.bind_property('text', filterModel.filter, 'search', 0);

    let vbox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
    vbox.append(searchEntry);
    vbox.append(overlay);

    let openButton = Gtk.Button.new_with_mnemonic(_("_Open"));
    openButton.connect('clicked', () => {
        let dialog = Gtk.FileChooserNative.new("Open file", openButton.get_root(), Gtk.FileChooserAction.OPEN, _("_Open"), _("_Cancel"));
        dialog.set_modal(true);
        dialog.set_current_folder(Gio.File.new_for_path('/usr/share/dict/'));

        dialog.connect('response', (dialog, responseId) => {
            let file = responseId == Gtk.ResponseType.ACCEPT ? dialog.get_file() : null;

            dialog.unref();
            dialog.destroy();

            if (file)
                loadFile(file, filterModel.model);
        });

        dialog.ref();
        dialog.show();
    });

    let window = new Gtk.Window({
        application,
        defaultWidth: 400, defaultHeight: 600,
        child: vbox,
    });

    let update = function() {
        let total = filterModel.model.get_n_items();
        let pending = filterModel.pending;
        let filtered = filterModel.get_n_items();

        progressBar.visible = !!filterModel.pending;
        progressBar.fraction = (total - pending) / (total || 1);
        window.title = _N("%d line", "%d lines", filtered).format(filtered);
    };

    filterModel.connect('items-changed', update);
    filterModel.connect('notify::pending', update);
    update();

    window.set_titlebar(new Gtk.HeaderBar());
    window.get_titlebar().pack_start(openButton);
    window.present();
});

application.run([]);
