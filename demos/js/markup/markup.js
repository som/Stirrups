// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Markup
 *
 * GtkTextBuffer lets you define your own tags that can influence
 * text formatting in a variety of ways. In this example, we show
 * that GtkTextBuffer can load Pango markup and automatically generate
 * suitable tags.
 */


imports.gi.versions['Gtk'] = '4.0';
const { Gio, Gtk } = imports.gi;
const ByteArray = imports.byteArray;
const directory = Gio.File.new_for_path('.');

let application = new Gtk.Application();

application.connect('activate', () => {
    let contents = directory.get_child('markup.txt').load_contents(null)[1];

    let stack = new Gtk.Stack();
    stack.add_named(new Gtk.ScrolledWindow({
        child: new Gtk.TextView({
            wrapMode: Gtk.WrapMode.WORD_CHAR,
            leftMargin: 10, rightMargin: 10,
            editable: false,
            buffer: new Gtk.TextBuffer(),
        }),
    }), 'formatted');
    stack.add_named(new Gtk.ScrolledWindow({
        child: new Gtk.TextView({
            wrapMode: Gtk.WrapMode.WORD,
            leftMargin: 10, rightMargin: 10,
            editable: true,
            buffer: new Gtk.TextBuffer({ text: ByteArray.toString(contents) }),
        }),
    }), 'source');

    let button = new Gtk.CheckButton({ label: "Source" });
    // Synchronize the 'formatted' buffer with the 'source' buffer.
    button.connect('toggled', () => {
        if (button.active) {
            stack.set_visible_child_name('source');
            return;
        }

        let markup = stack.get_child_by_name('source').child.buffer.text;
        let buffer = stack.get_child_by_name('formatted').child.buffer;
        let [start, end] = buffer.get_bounds();
        buffer.begin_irreversible_action();
        buffer.delete(start, end);
        buffer.insert_markup(start, markup, -1);
        buffer.end_irreversible_action();

        stack.set_visible_child_name('formatted');
    });
    // Init the synchronization.
    button.emit('toggled');

    let window = new Gtk.Window({
        application, title: "Markup",
        defaultWidth: 450, defaultHeight: 450,
        child: stack,
    });
    window.set_titlebar(new Gtk.HeaderBar());
    window.get_titlebar().pack_start(button);
    window.present();
});

application.run([]);
