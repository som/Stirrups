// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* exported PictureWidget */

const { Gdk, Gio, GObject, Graphene, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');

var PictureWidget = GObject.registerClass({
    GTypeName: 'Demo3Widget',
    Template: directory.get_child('demo3widget.ui').get_uri(),
    InternalChildren: ['menu'],
    Properties: {
        'paintable': GObject.ParamSpec.object(
            'paintable', "Paintable", "The picture paintable",
            GObject.ParamFlags.READWRITE, Gdk.Paintable.$gtype
        ),
        // The 'CONSTRUCT' flag makes the property notified even if it is not explicitly set.
        // So the 'zoom.reset' action 'enabled' property is updated at startup (false with the default scale value).
        'scale': GObject.ParamSpec.float(
            'scale', "Scale", "The picture zoom scale",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT, 0, 10, 1
        ),
    },
}, class extends Gtk.Widget {
    static newFromFile(file) {
        return new this({ paintable: Gdk.Texture.new_from_file(file) });
    }

    vfunc_constructed() {
        // These are the actions that we are using in the menu.
        // TODO: Report Gtk.Widget.install_action bug.
        let actionGroup = new Gio.SimpleActionGroup();
        ['in', 'out', 'reset'].forEach(name => {
            actionGroup.add_action(new Gio.SimpleAction({ name }));
            actionGroup.lookup_action(name).connect('activate', action => {
                this.scale = (
                    action.name == 'in' ? Math.min(10, this.scale * Math.SQRT2) :
                    action.name == 'out' ? Math.max(0.01, this.scale / Math.SQRT2) :
                    1
                );
            });
        });
        this.insert_action_group('zoom', actionGroup);

        super.vfunc_constructed();
    }

    vfunc_measure(orientation, forSize_) {
        let size = this.scale * (this.paintable?.[
            orientation == Gtk.Orientation.HORIZONTAL ?
                'get_intrinsic_width' : 'get_intrinsic_height'
        ]() ?? 0);

        return [size, size, -1, -1];
    }

    vfunc_notify(pspec) {
        switch (pspec.get_name()) {
            case 'paintable':
                this.queue_resize();
                break;
            case 'scale':
                this.queue_resize();
                this.action_set_enabled('zoom.in', this.scale < 10);
                this.action_set_enabled('zoom.out', this.scale > 0.01);
                this.action_set_enabled('zoom.reset', this.scale != 1);
        }
    }

    vfunc_size_allocate() {
        // Since we are not using a layout manager (who would do this for us),
        // we need to allocate a size for our menu by calling gtk_popover_present().
        this._menu.present();
    }

    vfunc_snapshot(snapshot) {
        let width = this.scale * this.paintable.get_intrinsic_width();
        let height = this.scale * this.paintable.get_intrinsic_height();
        let x = Math.max(0, (this.get_width() - Math.ceil(width)) / 2);
        let y = Math.max(0, (this.get_height() - Math.ceil(height)) / 2);

        snapshot.push_clip(new Graphene.Rect().init(0, 0, this.get_width(), this.get_height()));
        snapshot.save();
        snapshot.translate(new Graphene.Point().init(x, y));
        this.paintable.snapshot(snapshot, width, height);
        snapshot.restore();
        snapshot.pop();
    }

    // Do the 'dispose' stuff here because 'dispose' vfunc is not usable with GJS.
    vfunc_unrealize() {
        super.vfunc_unrealize();

        this.paintable = null;
        [...this].forEach(child => child.unparent());

        // XXX: Avoid an 'offending callback' GJS error with the 'notify' vfunc on widget disposed.
        this.unparent();
    }

    // Builder handler.
    pressed_cb(gesture_, nPress_, x, y) {
        // We are placing our menu at the point where the click happened, before popping it up.
        this._menu.set_pointing_to(new imports.gi.Gdk.Rectangle({ x, y, width: 1, height: 1 }));
        this._menu.popup();
    }
});
