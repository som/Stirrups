// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Menu
 *
 * Demonstrates how to add a context menu to a custom widget
 * and connect it with widget actions.
 *
 * The custom widget we create here is similar to a GtkPicture,
 * but allows setting a zoom level for the displayed paintable.
 *
 * Our context menu has items to change the zoom level.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, Gtk } = imports.gi;
const { PictureWidget } = imports.demo3widget;
const directory = Gio.File.new_for_path('.');

let application = new Gtk.Application();

application.connect('activate', () => {
    new Gtk.Window({
        application,
        title: "Menu",
        defaultWidth: 600, defaultHeight: 400,
        child: new Gtk.ScrolledWindow({
            child: PictureWidget.newFromFile(directory.get_child('portland-rose.jpg')),
        }),
    }).present();
});

application.run([]);
