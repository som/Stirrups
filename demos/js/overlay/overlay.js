// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Interactive Overlay
 *
 * Shows widgets in static positions over a main widget.
 *
 * The overlaid widgets can be interactive controls such
 * as the entry in this example, or just decorative, like
 * the big blue label.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gtk } = imports.gi;

let application = new Gtk.Application();

application.connect('activate', () => {
    let vbox, entry, grid, button, overlay = new Gtk.Overlay({
        child: grid = new Gtk.Grid(),
    });

    overlay.add_overlay(vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL, spacing: 10,
        canTarget: false,
        halign: Gtk.Align.CENTER, valign: Gtk.Align.START,
    }));

    vbox.append(new Gtk.Label({
        label: "<span foreground='blue' weight='ultrabold' font='40'>Numbers</span>", useMarkup: true,
        canTarget: false,
        marginTop: 8, marginBottom: 8,
    }));

    overlay.add_overlay(vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL, spacing: 10,
        halign: Gtk.Align.CENTER, valign: Gtk.Align.CENTER,
    }));

    vbox.append(entry = new Gtk.Entry({
        placeholderText: "Your Lucky Number",
        marginTop: 8, marginBottom: 8,
    }));

    for (let j = 0; j < 5; j++) {
        for (let i = 0; i < 5; i++) {
            grid.attach(button = new Gtk.Button({
                label: String(5 * j + i),
                hexpand: true, vexpand: true,
            }), i, j, 1, 1);

            button.connect('clicked', button => {
                entry.text = button.label;
            });
        }
    }

    new Gtk.Window({
        application, title: "Decorative Overlay",
        defaultWidth: 500, defaultHeight: 510,
        child: overlay,
        visible: true,
    });
});

application.run([]);
