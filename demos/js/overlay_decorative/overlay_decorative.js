// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Decorative Overlay
 *
 * Another example of an overlay with some decorative
 * and some interactive controls.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');

let application = new Gtk.Application();

application.connect('activate', () => {
    let textView = new Gtk.TextView({
        buffer: new Gtk.TextBuffer({ text: "Dear diary..." }),
    });

    let adjustment = Gtk.Adjustment.new(0, 0, 100, 1, 1, 0);
    adjustment.bind_property('value', textView, 'left-margin', 0);
    adjustment.bind_property('value', textView, 'top-margin', 0);
    adjustment.set_value(100);

    let overlay = new Gtk.Overlay({
        child: new Gtk.ScrolledWindow({ child: textView }),
    });

    overlay.add_overlay(new Gtk.Picture({
        file: directory.get_child('decor1.png'),
        halign: Gtk.Align.START, valign: Gtk.Align.START,
        canTarget: false,
    }));

    overlay.add_overlay(new Gtk.Picture({
        file: directory.get_child('decor2.png'),
        halign: Gtk.Align.END, valign: Gtk.Align.END,
        canTarget: false,
    }));

    overlay.add_overlay(new Gtk.Scale({
        adjustment, tooltipText: "Margin",
        halign: Gtk.Align.START, valign: Gtk.Align.END,
        marginStart: 20, marginEnd: 20, marginBottom: 20,
        widthRequest: 120,
    }));

    new Gtk.Window({
        application, title: "Interactive Overlay",
        defaultWidth: 500, defaultHeight: 510,
        child: overlay,
        visible: true,
    });
});

application.run([]);
