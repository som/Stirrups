// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Page Setup
 *
 * GtkPageSetupUnixDialog can be used if page setup is needed
 * independent of a full printing dialog.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gtk } = imports.gi;

let application = new Gtk.Application();

application.connect('activate', () => {
    new Gtk.PageSetupUnixDialog({
        application,
        title: "Page Setup",
        visible: true,
    }).connect('response', dialog => dialog.destroy());
});

application.run([]);
