// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/*
 * This class is shared by the "Animated Paintable" and the "Sliding Puzzle" demos.
 */

/* exported NuclearAnimation */

const { Gdk, GLib, GObject, Graphene } = imports.gi;

// Do a full rotation in 5 seconds. We will register the timeout
// for doing a single step to be executed every 10ms,
// which means after 1000 steps 10s will have elapsed.
const MAX_PROGRESS = 500;
const RADIUS = 0.3;

var NuclearAnimation = GObject.registerClass({
    Implements: [Gdk.Paintable],
}, class NuclearAnimation extends GObject.Object {
    _init(animate = true, progress = 0) {
        super._init();

        // This variable stores the progress of our animation. We just count
        // upwards until we hit MAX_PROGRESS and then start from scratch.
        this._progress = progress;

        if (!(this._animated = animate))
            return;

        // Add a timer here that constantly updates our animations. We want
        // to update it often enough to guarantee a smooth animation. Ideally,
        // we'd attach to the frame clock, but because we do not have it
        // available here, we just use a regular timeout that hopefully triggers
        // often enough to be smooth.
        GLib.timeout_add(GLib.PRIORITY_DEFAULT, 10, () => {
            this._progress = (this._progress + 1) % MAX_PROGRESS;

            // Now we need to tell all listeners that we've changed out contents
            // so that they can redraw this paintable.
            this.invalidate_contents();

            return GLib.SOURCE_CONTINUE;
        });
    }

    // For non-static paintables, this function needs to be implemented. It must return
    // a static paintable with the same contents as this one currently has.
    vfunc_get_current_image() {
        return this._animated ? new this.constructor(false, this._progress) : this;
    }

    // This time, we cannot set the static contents flag because our animation changes
    // the contents. However, our size still doesn't change, so report that flag.
    vfunc_get_flags() {
        return this._animated ? Gdk.PaintableFlags.SIZE :
            Gdk.PaintableFlags.CONTENTS | Gdk.PaintableFlags.SIZE;
    }

    // The same function as the "Paintable" demo one, plus the rotation.
    vfunc_snapshot(snapshot, width, height) {
        snapshot.append_color(
            new Gdk.RGBA({ red: 0.9, green: 0.75, blue: 0.15, alpha: 1.0 }),
            new Graphene.Rect({ size: { width, height } })
        );

        let min = Math.min(width, height);
        let cr = snapshot.append_cairo(
            new Graphene.Rect().init((width - min) / 2, (height - min) / 2, min, min)
        );

        cr.translate(width / 2, height / 2);
        cr.scale(min, min);
        cr.rotate(2 * Math.PI * this._progress / MAX_PROGRESS);

        cr.arc(0, 0, 0.1, -Math.PI, Math.PI);
        cr.fill();

        cr.setLineWidth(RADIUS);
        cr.setDash([RADIUS * Math.PI / 3], 0);
        cr.arc(0, 0, RADIUS, -Math.PI, Math.PI);
        cr.stroke();

        cr.$dispose();
    }
});
