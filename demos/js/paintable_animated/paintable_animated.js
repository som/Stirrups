// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Animated Paintable
 *
 * GdkPaintable also allows paintables to change.
 *
 * This demo code gives an example of how this could work.
 *
 * Paintables can also change their size, this works similarly, but
 * we will not demonstrate this here as our icon does not have any size.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gtk } = imports.gi;
const { NuclearAnimation } = imports.nuclearanimation;

let application = new Gtk.Application();

application.connect('activate', () => {
    new Gtk.Window({
        application, title: "Nuclear Animation",
        defaultWidth: 300, defaultHeight: 200,
        child: new Gtk.Image({ paintable: new NuclearAnimation() }),
    }).present();
});

application.run([]);
