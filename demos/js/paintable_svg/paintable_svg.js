// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* SVG
 *
 * This demo shows wrapping a librsvg RsvgHandle in a GdkPaintable
 * to display an SVG image that can be scaled by resizing the window.
 *
 * This demo relies on librsvg, which GTK itself does not link against.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, Gio, GObject, Graphene, Gtk, Rsvg } = imports.gi;
const _ = imports.gettext.domain('gtk40').gettext;
const directory = Gio.File.new_for_path('.');

const SVGPaintable = GObject.registerClass({
    Implements: [Gdk.Paintable],
    Properties: {
        'file': GObject.ParamSpec.object(
            'file', "File", "The file this renders",
            GObject.ParamFlags.READWRITE, Gio.File.$gtype
        ),
    },
}, class SVGPaintable extends GObject.Object {
    // Inform the widget that this prefers the SVG height.
    vfunc_get_intrinsic_height() {
        return this._handle?.get_dimensions().height ?? 0;
    }

    // Inform the widget that this prefers the SVG width.
    vfunc_get_intrinsic_width() {
        return this._handle?.get_dimensions().width ?? 0;
    }

    // Create a RSVG handle when a new file is set.
    vfunc_notify(pspec) {
        if (pspec.get_name() == 'file') {
            this._handle = this.file ?
                Rsvg.Handle.new_from_gfile_sync(this.file, Rsvg.HandleFlags.FLAGS_NONE, null) : null;
            this._handle?.set_dpi(90);

            // Inform the widget that 'get_intrinsic_height', 'get_intrinsic_width' and 'snapshot'
            // result/behaviour will be different.
            this.invalidate_contents();
            this.invalidate_size();
        }
    }

    // Draw the SVG thanks to Cairo and RSVG.
    vfunc_snapshot(snapshot, width, height) {
        if (!this._handle)
            return;

        let cr = snapshot.append_cairo(new Graphene.Rect({ size: { width, height } }));

        try {
            this._handle.render_document(cr, new Rsvg.Rectangle({ width, height }));
        } catch(e) {
            logError(e);
        }

        cr.$dispose();
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let button = Gtk.Button.new_with_mnemonic(_("_Open"));
    button.connect('clicked', button => {
        let dialog = Gtk.FileChooserNative.new("Open node file", button.root, Gtk.FileChooserAction.OPEN, null, null);
        dialog.filter = new Gtk.FileFilter();
        dialog.filter.add_mime_type('image/svg+xml');
        dialog.modal = true;

        dialog.connect('response', (dialog, responseId) => {
            let file = responseId == Gtk.ResponseType.ACCEPT ? dialog.get_file() : null;
            let window = dialog.transientFor;

            dialog.unref();
            dialog.destroy();

            if (file)
                window.child.paintable.file = file;
        });

        dialog.ref();
        dialog.show();
    });

    let window = new Gtk.Window({
        application, title: "Paintable — SVG",
        defaultWidth: 330, defaultHeight: 330,
        child: new Gtk.Picture({
            // The minimum size of the picture.
            widthRequest: 16, heightRequest: 16,
            // The picture can be smaller than the paintable preferred size, that is the SVG size here.
            canShrink: true,
            paintable: new SVGPaintable({
                file: directory.get_child('org.gtk.gtk4.NodeEditor.Devel.svg'),
            }),
        }),
    });

    window.set_titlebar(new Gtk.HeaderBar());
    window.get_titlebar().pack_start(button);
    window.present();
});

application.run([]);
