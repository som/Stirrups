// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Pickers
 *
 * These widgets are mainly intended for use in preference dialogs.
 * They allow to select colors, fonts, directories and applications.
 *
 * This demo shows both the default appearance for these dialogs,
 * as well as some of the customizations that are possible.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, Gtk } = imports.gi;

const ALIAS_FONT_FAMILIES = ['Cursive', 'Fantasy', 'Monospace', 'Sans', 'Serif', 'System-ui'];

const SOLARIZED_COLORS = [
    '#ffffff', '#073642', '#dc322f', '#859900', '#b58900', '#268bd2', '#d33682', '#2aa198', '#eee8d5',
    '#000000', '#002b36', '#cb4b16', '#586e75', '#657b83', '#839496', '#6c71c4', '#93a1a1', '#fdf6e3',
].map(hexaString => {
    let color = new Gdk.RGBA();
    color.parse(hexaString);
    return color;
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let grid = new Gtk.Grid({
        marginStart: 20, marginEnd: 20,
        marginTop: 20, marginBottom: 20,
        rowSpacing: 3, columnSpacing: 10,
    });

    grid.attach(
        new Gtk.Label({ label: "Standard", cssClasses: ['title-4'] }),
        1, -1, 1, 1
    );
    grid.attach(
        new Gtk.Label({ label: "Custom", cssClasses: ['title-4'] }),
        2, -1, 1, 1
    );

    grid.attach(
        new Gtk.Label({
            label: "Color:",
            halign: Gtk.Align.START, valign: Gtk.Align.CENTER,
            hexpand: true,
        }),
        0, 0, 1, 1
    );
    grid.attach(
        new Gtk.ColorButton(),
        1, 0, 1, 1
    );
    grid.attach(
        new Gtk.ColorButton({ title: "Solarized colors" }),
        2, 0, 1, 1
    );
    grid.get_child_at(2, 0).add_palette(Gtk.Orientation.HORIZONTAL, 9, SOLARIZED_COLORS);

    grid.attach(
        new Gtk.Label({
            label: "Font:",
            halign: Gtk.Align.START, valign: Gtk.Align.CENTER,
            hexpand: true,
        }),
        0, 1, 1, 1
    );
    grid.attach(
        new Gtk.FontButton(),
        1, 1, 1, 1
    );
    grid.attach(
        new Gtk.FontButton({ level: Gtk.FontChooserLevel.FAMILY | Gtk.FontChooserLevel.SIZE }),
        2, 1, 1, 1
    );
    grid.get_child_at(2, 1).set_filter_func(fontFamily => {
        return ALIAS_FONT_FAMILIES.includes(fontFamily.get_name());
    });
    // Do not make GJS cry on window closed.
    grid.get_child_at(2, 1).connect('unrealize', fontButton => {
        fontButton.set_filter_func(null);
    });

    grid.attach(
        new Gtk.Label({
            label: "Mail:",
            halign: Gtk.Align.START, valign: Gtk.Align.CENTER,
            hexpand: true,
        }),
        0, 2, 1, 1
    );
    grid.attach(
        new Gtk.AppChooserButton({
            contentType: 'x-scheme-handler/mailto',
            showDialogItem: true
        }),
        1, 2, 1, 1
    );

    new Gtk.Window({
        application,
        title: "Pickers",
        child: grid,
    }).present();
});

application.run([]);
