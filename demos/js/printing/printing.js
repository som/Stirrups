// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Printing
 *
 * GtkPrintOperation offers a simple API to support printing
 * in a cross-platform way.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GObject, Gtk, Pango, PangoCairo } = imports.gi;
const ByteArray = imports.byteArray;
const directory = Gio.File.new_for_path('.');

// In points unit;
const CODE_FONT_SIZE = 12;
const HEADER_FONT_SIZE = 14;
const HEADER_HEIGHT = 10 * 72 / 25.4;
const HEADER_GAP = 3 * 72 / 25.4;

const DemoPrintOperation = GObject.registerClass({
    Properties: {
        'source-code': GObject.ParamSpec.string(
            'source-code', "Source Code", "The source code to print",
            GObject.ParamFlags.READWRITE, null
        ),
        'source-title': GObject.ParamSpec.string(
            'source-title', "Source Title", "The title of the source",
            GObject.ParamFlags.READWRITE, null
        ),
    },
}, class DemoPrintOperation extends Gtk.PrintOperation {
    vfunc_begin_print(printContext) {
        let height = printContext.get_height() - HEADER_HEIGHT - HEADER_GAP;
        this._linesPerPage = Math.floor(height / CODE_FONT_SIZE);
        this._lines = this.sourceCode.split('\n');
        this.nPages = (this._lines.length - 1) / this._linesPerPage + 1;
    }

    vfunc_draw_page(printContext, pageNumber) {
        let cr = printContext.get_cairo_context();
        let width = printContext.get_width();

        // Draw the page header rectangle.
        cr.rectangle(0, 0, width, HEADER_HEIGHT);
        cr.setSourceRGB(0.8, 0.8, 0.8);
        cr.fillPreserve();
        cr.setSourceRGB(0, 0, 0);
        cr.setLineWidth(1);
        cr.stroke();

        // Fill the page header.
        {
            let textWidth, textHeight, layout = printContext.create_pango_layout();
            layout.set_font_description(Pango.FontDescription.from_string(`sans ${HEADER_FONT_SIZE}`));

            layout.set_text(this.sourceTitle, -1);
            [textWidth, textHeight] = layout.get_pixel_size();
            if (textWidth > width) {
                layout.set_width(width);
                layout.set_ellipsize(Pango.EllipsizeMode.START);
                [textWidth, textHeight] = layout.get_pixel_size();
            }
            cr.moveTo((width - textWidth) / 2, (HEADER_HEIGHT - textHeight) / 2);
            PangoCairo.show_layout(cr, layout);

            layout.set_text(`${pageNumber + 1} / ${this.nPages}`, -1);
            layout.set_width(-1);
            [textWidth, textHeight] = layout.get_pixel_size();
            cr.moveTo(width - textWidth - 4, (HEADER_HEIGHT - textHeight) / 2);
            PangoCairo.show_layout(cr, layout);
        }

        // Fill the page body.
        {
            let layout = printContext.create_pango_layout();
            layout.set_font_description(Pango.FontDescription.from_string(`monospace ${CODE_FONT_SIZE}`));

            cr.moveTo(0, HEADER_HEIGHT + HEADER_GAP);

            this._lines.slice(pageNumber * this._linesPerPage, (pageNumber + 1) * this._linesPerPage).forEach(line => {
                layout.set_text(line, -1);
                PangoCairo.show_layout(cr, layout);
                cr.relMoveTo(0, CODE_FONT_SIZE);
            });
        }

        cr.$dispose();
    }

    // Undo what has been done in vfunc_begin_print.
    // Quite useless here; keep it for demonstration purpose.
    vfunc_end_print(printContext) {
        delete this._linesPerPage;
        delete this._lines;
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let contents = directory.get_child('printing.js').load_contents(null)[1];
    let buffer = new Gtk.TextBuffer({ text: ByteArray.toString(contents) });

    let printSettings = new Gtk.PrintSettings();
    printSettings.set(Gtk.PRINT_SETTINGS_OUTPUT_BASENAME, 'stirrups-printing-demo');

    let button = new Gtk.Button({ iconName: 'document-print-symbolic' });
    button.connect('clicked', button => {
        let printOperation = new DemoPrintOperation({
            embedPageSetup: true,
            useFullPage: false,
            unit: Gtk.Unit.POINTS,
            printSettings,
            sourceCode: buffer.text,
            sourceTitle: "/demos/js/printing/printing.js",
        });

        try {
            printOperation.run(Gtk.PrintOperationAction.PRINT_DIALOG, button.root);
        } catch(e) {
            new Gtk.MessageDialog({
                transientFor: button.root, destroyWithParent: true, modal: true,
                messageType: Gtk.MessageType.ERROR,
                buttons: Gtk.ButtonsType.CLOSE,
                text: e.message,
                visible: true,
            }).connect('response', dialog => dialog.destroy());
        }
    });

    let window = new Gtk.Window({
        application,
        title: "Printing",
        defaultWidth: 450, defaultHeight: 450,
        child: new Gtk.ScrolledWindow({
            child: new Gtk.TextView({
                wrapMode: Gtk.WrapMode.WORD_CHAR,
                leftMargin: 10, rightMargin: 10,
                topMargin: 10, bottomMargin: 10,
                buffer,
            }),
        }),
    });
    window.set_titlebar(new Gtk.HeaderBar());
    window.get_titlebar().pack_end(button);
    window.present();
});

application.run([]);
