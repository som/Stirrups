// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Revealer
 *
 * GtkRevealer is a container that animates showing and hiding
 * of its sole child with nice transitions.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GLib, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');

let application = new Gtk.Application();

application.connect('activate', () => {
    let builder = Gtk.Builder.new_from_file(directory.get_child('revealer.ui').get_path());
    let window = builder.get_object('window');

    let count = 0;
    let timeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 690, () => {
        let revealer = builder.get_object(`revealer${count}`);
        revealer.set_reveal_child(true);
        revealer.connect('notify::child-revealed', () => {
            if (revealer.get_mapped())
                revealer.set_reveal_child(!revealer.childRevealed);
        });

        count++;

        if (count >= 9) {
            timeout = 0;
            return GLib.SOURCE_REMOVE;
        }

        return GLib.SOURCE_CONTINUE;
    });

    // Quite useless here.
    window.connect('close-request', () => {
        if (timeout)
            GLib.source_remove(timeout);
    });

    application.add_window(window);
    window.present();
});

application.run([]);
