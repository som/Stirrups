// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Search Entry
 *
 * GtkEntry allows to display icons and progress information.
 * This demo shows how to use these features in a search entry.
 *
 * Difference with GTK4 Demos: use a stack instead of a notebook.
 * (XXX: Why a notebook❓️)
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GLib, GObject, Gtk } = imports.gi;
const _ = imports.gettext.domain('gtk40').gettext;

// Useless so indispensable.
Gtk.Box.prototype.with = function(...children) {
    children.forEach(child => this.append(child));
    return this;
};
Gtk.Stack.prototype.with = function(...children) {
    children.forEach(child => this.add_child(child));
    return this;
};

// An entry with a context menu that exposes the search options.
const DemoEntry = GObject.registerClass(class extends Gtk.Entry {
    _init(params) {
        super._init({
            primaryIconName: 'edit-find-symbolic',
            primaryIconActivatable: true,
            primaryIconSensitive: false,
        });

        // The 'clear' action.
        let clearAction = Gio.SimpleAction.new('clear', null);
        clearAction.connect('activate', () => this.set_text(""));
        this.connect('notify::text', () => {
            clearAction.enabled = !!this.textLength;
        });
        this.notify('text');

        // The 'search-by' action.
        let searchByAction = Gio.SimpleAction.new_stateful('search-by', GLib.VariantType.new('s'), GLib.Variant.new_string('name'));
        searchByAction.connect('change-state', (searchByAction, variant) => {
            // ⚠️ When connecting to the 'change-state' signal, the default behaviour is overridden
            // so the new action state has to be set here.
            searchByAction.set_state(variant);

            switch (variant.get_string()[0]) {
                case 'name':
                    this.placeholderText = "Name…";
                    this.primaryIconTooltipText = "Search by name";
                    break;
                case 'description':
                    this.placeholderText = "Description…";
                    this.primaryIconTooltipText = "Search by description";
                    break;
                case 'filename':
                    this.placeholderText = "File name…";
                    this.primaryIconTooltipText = "Search by file name";
            }
        });
        searchByAction.emit('change-state', searchByAction.state);
        this.connect('icon-press', () => {
            // Cycle the search options.
            switch (searchByAction.state.get_string()[0]) {
                case 'name':
                    searchByAction.state = GLib.Variant.new_string('description');
                    break;
                case 'description':
                    searchByAction.state = GLib.Variant.new_string('filename');
                    break;
                case 'filename':
                    searchByAction.state = GLib.Variant.new_string('name');
            }
        });

        // Associate the actions with the entry.
        let actionGroup = new Gio.SimpleActionGroup();
        actionGroup.add_action(clearAction);
        actionGroup.add_action(searchByAction);
        this.insert_action_group('search', actionGroup);

        // Expose the actions in the entry's context menu.
        let menu = new Gio.Menu();
        let item = Gio.MenuItem.new(_("C_lear"), 'search.clear');
        item.set_attribute_value('touch-icon', GLib.Variant.new_string('edit-clear-symbolic'));
        menu.append_item(item);
        let submenu = new Gio.Menu();
        submenu.append(_("Name"), 'search.search-by::name');
        submenu.append(_("Description"), 'search.search-by::description');
        submenu.append(_("File Name"), 'search.search-by::filename');
        menu.append_submenu(_("Search By"), submenu);
        this.set_extra_menu(menu);
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let entry, findButton, cancelButton;
    let searchProgressId = 0;
    let finishSearchId = 0;

    // The search entry that is not a GtkSearchEntry such as the demo title induces.
    (entry = new DemoEntry()).connect('activate', () => {
        if (!searchProgressId)
            findButton.emit('clicked');
    });

    // A button to trigger a progress animation.
    (findButton = Gtk.Button.new_with_label("Find")).connect('clicked', button => {
        button.parent.visibleChild = cancelButton;

        searchProgressId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 1, () => {
            entry.progressFraction = 0.1;

            searchProgressId = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 100, () => {
                entry.progress_pulse();
                return GLib.SOURCE_CONTINUE;
            });

            return GLib.SOURCE_REMOVE;
        });

        finishSearchId = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 15, () => {
            if (searchProgressId) {
                GLib.source_remove(searchProgressId);
                searchProgressId = 0;
                entry.progressFraction = 0;
            }

            button.parent.visibleChild = findButton;
            finishSearchId = 0;
            return GLib.SOURCE_REMOVE;
        });
    });

    // A button to stop the progress animation.
    (cancelButton = Gtk.Button.new_with_label("Cancel")).connect('clicked', button => {
        button.parent.visibleChild = findButton;

        if (searchProgressId) {
            GLib.source_remove(searchProgressId);
            searchProgressId = 0;
            entry.progressFraction = 0;
        }

        if (finishSearchId) {
            GLib.source_remove(finishSearchId);
            finishSearchId = 0;
        }
    });

    new Gtk.Window({
        application, title: "Search Entry",
        resizable: false,
        child: new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            marginTop: 18, marginBottom: 18,
            marginStart: 18, marginEnd: 18,
            spacing: 12,
        }).with(
            new Gtk.Box({ spacing: 12 }).with(
                entry,
                new Gtk.Stack().with(findButton, cancelButton)
            )
        ),
    }).present();
});

application.run([]);
