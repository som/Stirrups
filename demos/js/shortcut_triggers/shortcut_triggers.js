// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Shortcuts
 *
 * GtkShortcut is the abstraction used by GTK to handle shortcuts from
 * keyboard or other input devices.
 *
 * Shortcut triggers can be used to weave complex sequences of key
 * presses into sophisticated mechanisms to activate shortcuts.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, Gio, GLib, Gtk } = imports.gi;

let application = new Gtk.Application();

application.connect('activate', () => {
    let listBox = new Gtk.ListBox({
        marginStart: 6, marginEnd: 6,
        marginTop: 6, marginBottom: 6,
    });

    [
        { description: "Press Ctrl-G", keyval: Gdk.KEY_g, modifiers: Gdk.ModifierType.CONTROL_MASK },
        { description: "Press X", keyval: Gdk.KEY_x, modifiers: 0 },
    ].forEach(o => {
        let { description, keyval, modifiers } = o;

        let row = new Gtk.ListBoxRow({
            actionName: 'win.info',
            actionTarget: GLib.Variant.new_string(`"${description}" row activated`),
            child: new Gtk.Label({ label: description }),
        });
        let controller = new Gtk.ShortcutController({ scope: Gtk.ShortcutScope.GLOBAL });
        controller.add_shortcut(new Gtk.Shortcut({
            trigger: Gtk.KeyvalTrigger.new(keyval, modifiers),
            action: Gtk.CallbackAction.new(() => {
                row.root.lookup_action('info').change_state(GLib.Variant.new_boolean(true));
                row.emit('activate');
                row.root.lookup_action('info').change_state(GLib.Variant.new_boolean(false));
            })
        }));
        row.child.add_controller(controller);
        listBox.insert(row, -1);
    });

    let infoBar = new Gtk.InfoBar({
        messageType: Gtk.MessageType.INFO,
        revealed: false, showCloseButton: true,
    });
    infoBar.connect('response', () => infoBar.set_revealed(false));
    let infoImage = new Gtk.Image();
    let infoLabel = new Gtk.Label();
    let infoBox = new Gtk.Box({ spacing: 8 });
    infoBox.append(infoImage);
    infoBox.append(infoLabel);
    infoBar.add_child(infoBox);

    let infoAction = new Gio.SimpleAction({
        name: 'info',
        parameterType: GLib.VariantType.new('s'),
        state: GLib.Variant.new_boolean(false),
    });
    infoAction.connect('activate', (action, parameter) => {
        infoLabel.set_label(parameter.unpack());
        infoImage.set_from_icon_name(action.state.unpack() ? 'input-keyboard-symbolic' : 'input-mouse-symbolic');
        infoBar.set_revealed(true);

        if (infoBar.timeout)
            GLib.source_remove(infoBar.timeout);
        infoBar.timeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 3000, () => {
            infoBar.set_revealed(false);
            infoBar.timeout = false;
            return GLib.SOURCE_REMOVE;
        });
    });
    infoBar.connect('unmap', () => {
        if (infoBar.timeout)
            GLib.source_remove(infoBar.timeout);
    });

    let vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        spacing: 12,
        marginStart: 12, marginEnd: 12,
        marginTop: 12, marginBottom: 12,
    });
    vbox.append(listBox);
    vbox.append(infoBar);

    let window = new Gtk.ApplicationWindow({
        application,
        title: "Shortcuts",
        resizable: false,
        defaultWidth: 300,
        child: vbox,
    });
    window.add_action(infoAction);
    window.present();
});

application.run([]);
