// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Size Groups
 *
 * GtkSizeGroup provides a mechanism for grouping a number of
 * widgets together so they all request the same amount of space.
 * This is typically useful when you want a column of widgets to
 * have the same size, but you can't use a GtkGrid widget.
 *
 * Note that size groups only affect the amount of space requested,
 * not the size that the widgets finally receive. If you want the
 * widgets in a GtkSizeGroup to actually be the same size, you need
 * to pack them in such a way that they get the size they request
 * and not more. For example, if you are packing your widgets
 * into a table, you would not include the GTK_FILL flag.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gtk } = imports.gi;

const TABLES = [
    {
        title: "Color Options",
        rows: [
            { title: "_Foreground", options: ["Red", "Green", "Blue"] },
            { title: "_Background", options: ["Red", "Green", "Blue"] },
        ]
    }, {
        title: "Line Options",
        rows: [
            { title: "_Dashing", options: ["Solid", "Dashed", "Dotted"] },
            { title: "_Line ends", options: ["Square", "Round", "Double Arrow"] },
        ]
    }
];

let application = new Gtk.Application();

application.connect('activate', () => {
    let vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        marginStart: 5, marginEnd: 5,
        marginTop: 5, marginBottom: 5,
        spacing: 5,
    });

    let sizeGroup = new Gtk.SizeGroup();

    // Create frames holding color and line options.
    TABLES.forEach(table => {
        let grid = new Gtk.Grid({
            marginStart: 5, marginEnd: 5,
            marginTop: 5, marginBottom: 5,
            rowSpacing: 5, columnSpacing: 10,
        });

        table.rows.forEach((row, index) => {
            let combo = new Gtk.ComboBoxText({
                halign: Gtk.Align.END, valign: Gtk.Align.BASELINE,
            });
            row.options.forEach(option => combo.append_text(option));
            combo.set_active(0);

            let label = new Gtk.Label({
                halign: Gtk.Align.START, valign: Gtk.Align.BASELINE,
                hexpand: true,
                label: row.title,
                useUnderline: true,
                mnemonicWidget: combo,
            });

            grid.attach(label, 0, index, 1, 1);
            grid.attach(combo, 1, index, 1, 1);
            sizeGroup.add_widget(combo);
        });

        vbox.append(new Gtk.Frame({
            label: table.title,
            child: grid,
        }));
    });

    // Create a check button to turn grouping on and off.
    let checkButton = new Gtk.CheckButton({
        label: "_Enable grouping",
        useUnderline: true,
        active: true,
    });
    vbox.append(checkButton);

    // GTK_SIZE_GROUP_NONE is not generally useful, but is useful here
    // to show the effect of GTK_SIZE_GROUP_HORIZONTAL by contrast.
    sizeGroup.mode = Gtk.SizeGroupMode[checkButton.active ? 'HORIZONTAL' : 'NONE'];
    checkButton.connect('toggled', () => {
        sizeGroup.mode = Gtk.SizeGroupMode[checkButton.active ? 'HORIZONTAL' : 'NONE'];
    });

    new Gtk.Window({
        application,
        title: "Size Groups",
        resizable: false,
        child: vbox,
    }).present();
});

application.run([]);
