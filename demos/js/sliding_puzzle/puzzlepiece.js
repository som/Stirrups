/* exported PuzzlePiece */

const { Gdk, GLib, GObject, Graphene } = imports.gi;

var PuzzlePiece = GObject.registerClass({
    Implements: [Gdk.Paintable],
    Properties: {
        'h-position': GObject.ParamSpec.uint(
            'h-position', "Horizontal position", "The horizontal position, relative to the horizontal total",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, 0, GLib.MAXUINT32, 0
        ),
        'v-position': GObject.ParamSpec.uint(
            'v-position', "Vertical position", "The vertical position, relative to the vertical total",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, 0, GLib.MAXUINT32, 0
        ),
        'h-total': GObject.ParamSpec.uint(
            'h-total', "Horizontal total", "The horizontal total",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, 0, GLib.MAXUINT32, 0
        ),
        'v-total': GObject.ParamSpec.uint(
            'v-total', "Vertical total", "The vertical total",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, 0, GLib.MAXUINT32, 0
        ),
        'source-paintable': GObject.ParamSpec.object(
            'source-paintable', "Source paintable", "The whole puzzle paintable whose this is a piece",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT, Gdk.Paintable.$gtype
        ),
    },
}, class PuzzlePiece extends GObject.Object {
    _init(params) {
        if (params.hPosition > params.hTotal || params.vPosition > params.vTotal)
            throw new Error("Invalid parameters");

        super._init(params);
    }

    get sourcePaintable() {
        return this._sourcePaintable ?? null;
    }

    // Bind the 'invalidate-contents' and 'invalidate-size' signals.
    set sourcePaintable(sourcePaintable) {
        if (this.sourcePaintable == sourcePaintable)
            return;

        if (this.sourcePaintable) {
            this.sourcePaintable.disconnect(this._contentsHandler);
            this.sourcePaintable.disconnect(this._sizeHandler);
        }

        if (sourcePaintable) {
            this._contentsHandler = sourcePaintable.connect('invalidate-contents', () => {
                this.invalidate_contents();
            });

            this._sizeHandler = sourcePaintable.connect('invalidate-size', () => {
                this.invalidate_size();
            });
        }

        this._sourcePaintable = sourcePaintable;
        this.notify('source-paintable');
    }

    // The flags are the same as the ones of the puzzle.
    // If the puzzle changes in some way, so do the pieces.
    vfunc_get_flags() {
        return this.sourcePaintable?.get_flags() ?? 0;
    }

    // We can compute our width relative to the puzzle. This logic even works for the
    // case where the puzzle has no width, because the 0 return value is unchanged.
    // Round up the value.
    vfunc_get_intrinsic_width() {
        if (!this.sourcePaintable)
            return 0;

        return (this.sourcePaintable.get_intrinsic_width() + this.hTotal - 1) / this.hTotal;
    }

    // Do the same thing we did for the width with the height.
    vfunc_get_intrinsic_height() {
        if (!this.sourcePaintable)
            return 0;

        return (this.sourcePaintable.get_intrinsic_height() + this.vTotal - 1) / this.vTotal;
    }

    // We can compute our aspect ratio relative to the puzzle. This logic again works
    // for the case where the puzzle has no aspect ratio, because the 0 return value
    // is unchanged.
    vfunc_get_intrinsic_aspect_ratio() {
        if (!this.sourcePaintable)
            return 0;

        return this.sourcePaintable.get_intrinsic_aspect_ratio() * this.vTotal / this.hTotal;
    }

    // This is the function that draws the puzzle piece. It just draws a rectangular
    // cutout of the puzzle by clipping away the rest.
    vfunc_snapshot(snapshot, width, height) {
        if (!this.sourcePaintable)
            return;

        snapshot.push_clip(new Graphene.Rect({ size: { width, height } }));
        snapshot.translate(new Graphene.Point({ x: -width * this.hPosition, y: -height * this.vPosition }));
        this.sourcePaintable.snapshot(snapshot, width * this.hTotal, height * this.vTotal);
        snapshot.pop();
    }

    // Disconnect the signal handlers from the source paintable, so this can be disposed
    // by the garbage collector.
    destroy() {
        this.sourcePaintable = null;
    }
});
