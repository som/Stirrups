// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Spinner
 *
 * GtkSpinner allows to show that background activity is on-going.
 */

imports.gi.versions['Gtk'] = '4.0';
const Gtk = imports.gi.Gtk;
const _ = imports.gettext.domain('gtk40').gettext;

let application = new Gtk.Application();

application.connect('activate', () => {
    let sensitiveSpinner = new Gtk.Spinner();
    let unsensitiveSpinner = new Gtk.Spinner({ sensitive: false });

    let vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        spacing: 5,
        marginStart: 5, marginEnd: 5,
        marginTop: 5, marginBottom: 5,
    });

    [sensitiveSpinner, unsensitiveSpinner].forEach(spinner => {
        let hbox = new Gtk.Box({ spacing: 5 });
        hbox.append(spinner);
        hbox.append(new Gtk.Entry());
        vbox.append(hbox);
    });

    [[_("Play"), true], [_("Stop"), false]].forEach(([label, spinning]) => {
        let button = new Gtk.Button({ label });
        button.connect('clicked', () => {
            sensitiveSpinner.spinning = spinning;
            unsensitiveSpinner.spinning = spinning;
        });
        vbox.append(button);
    });

    sensitiveSpinner.start();
    unsensitiveSpinner.start();

    let dialog = new Gtk.Dialog({
        application,
        title: "Spinner",
        resizable: false,
    });
    dialog.add_button(_("_Close"), Gtk.ResponseType.NONE);
    dialog.get_content_area().append(vbox);

    dialog.connect('response', () => dialog.destroy());
    dialog.show();
});

application.run([]);
