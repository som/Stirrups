// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Tagged Entry
 *
 * This example shows how to build a complex composite
 * entry using GtkText, outside of GTK.
 *
 * This tagged entry can display tags and other widgets
 * inside the entry area.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, Gio, Gtk } = imports.gi;
const { Tag, TaggedEntry } = imports.demotaggedentry;
const stylesheet = Gio.File.new_for_path('.').get_child('tagstyle.css');

let application = new Gtk.Application();

application.connect('activate', () => {
    let entry = new TaggedEntry();

    let addButton = Gtk.Button.new_with_mnemonic("Add _Tag");
    addButton.connect('clicked', () => {
        let tag = new Tag({
            label: "Blue", cssClasses: ['blue'],
            hasCloseButton: true,
        });
        tag.connect('button-clicked', () => entry.remove(tag));

        if (entry.get_last_child() instanceof Gtk.Spinner)
            entry.insertChildAfter(tag, entry.get_last_child().get_prev_sibling());
        else
            entry.addChild(tag);
    });

    let spinnerButton = Gtk.CheckButton.new_with_mnemonic("_Spinner");
    spinnerButton.connect('toggled', () => {
        // Should not happen.
        if (spinnerButton.active == (entry.get_last_child() instanceof Gtk.Spinner))
            return;

        if (spinnerButton.active) {
            let spinner = new Gtk.Spinner();
            spinner.start();
            entry.addChild(spinner);
        } else {
            entry.remove(entry.get_last_child());
        }
    });

    let hbox = new Gtk.Box({
        halign: Gtk.Align.END,
        spacing: 6,
    });
    hbox.append(addButton);
    hbox.append(spinnerButton);

    let vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        marginTop: 18, marginBottom: 18,
        marginStart: 18, marginEnd: 18,
        spacing: 6,
    });
    vbox.append(entry);
    vbox.append(hbox);

    let provider = new Gtk.CssProvider();
    provider.load_from_file(stylesheet);
    Gtk.StyleContext.add_provider_for_display(Gdk.Display.get_default(), provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

    new Gtk.Window({
        application, title: "Tagged Entry",
        widthRequest: 260, resizable: false,
        child: vbox,
    }).present();
});

application.run([]);
