// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Themes
 *
 * This demo continuously switches themes, like some of you.
 *
 * Warning: Although the flashing changes this demo involves are less rapid than
 * those of the original GTK demo, it may be harmful for photosensitive viewers.
 *
 * Press Escape to quit the demo.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GLib, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');
const vprintf = imports.format.vprintf;

const themes = [
    { name: 'Adwaita' },
    { name: 'Adwaita', dark: true },
    { name: 'HighContrast' },
    { name: 'HighContrastInverse' },
];

let application = new Gtk.Application();

application.add_action(new Gio.SimpleAction({ name: 'quit' }));
application.lookup_action('quit').connect('activate', () => application.quit());
application.set_accels_for_action('app.quit', ['Escape']);

application.connect('activate', () => {
    let builder = Gtk.Builder.new_from_file(directory.get_child('themes.ui').get_path());
    let label = builder.get_object('fps');
    let button = builder.get_object('toggle');
    let warning = builder.get_object('warning');
    let window = builder.get_object('window');

    let timeoutId = 0;
    let themeIndex = 0;

    button.connect('notify::active', () => {
        if (button.active && !timeoutId) {
            warning.present();
        } else if (!button.active && timeoutId) {
            GLib.source_remove(timeoutId);
            timeoutId = 0;
        }
    });

    warning.connect('response', (warning, responseId) => {
        warning.hide();

        if (responseId == Gtk.ResponseType.OK)
            // Not to use tick callback since it makes the demo viewing
            // very uncomfortable and harmful.
            timeoutId = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 500, () => {
                let theme = themes[themeIndex++ % themes.length];
                Object.assign(Gtk.Settings.get_default(), {
                    gtkThemeName: theme.name,
                    gtkApplicationPreferDarkTheme: theme.dark,
                });
                window.title = theme.name + (theme.dark ? " (dark)" : "");

                let fps = window.get_frame_clock().get_fps();
                label.label = vprintf("%.2f fps", [fps]);

                return GLib.SOURCE_CONTINUE;
            });
        else
            button.active = false;
    });

    window.set_application(application);
    window.present();
});

application.run([]);
