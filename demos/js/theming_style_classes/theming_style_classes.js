// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Style Classes
 *
 * GTK uses CSS for theming. Style classes can be associated
 * with widgets to inform the theme about intended rendering.
 *
 * This demo shows some common examples where theming features
 * of GTK are used for certain effects: primary toolbars
 * and linked buttons.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');

let application = new Gtk.Application();

application.connect('activate', () => {
    let builder = Gtk.Builder.new_from_file(directory.get_child('theming.ui').get_path());
    let grid = builder.get_object('grid');

    new Gtk.Window({
        application,
        title: "Style Classes",
        resizable: false,
        child: grid,
    }).present();
});

application.run([]);
