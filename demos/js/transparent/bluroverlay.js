// SPDX-FileCopyrightText: 2011 Ignacio Casal Quinteiro
// SPDX-FileCopyrightText: 2011 Mike Krüger
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* exported BlurOverlay */

const { Gdk, GObject, Graphene, Gtk } = imports.gi;
const Cairo = imports.cairo;
const System = imports.system;

Object.defineProperty(Object.prototype, 'toBounds', {
    value: function() {
        let { x, y, width, height } = this;
        return new Graphene.Rect().init(x, y, width, height);
    }
});

Object.defineProperty(Gtk.Widget.prototype, 'effectiveHalign', {
    get() {
        switch (this.halign) {
            case Gtk.Align.START:
                return this.get_direction() == Gtk.TextDirection.RTL ? Gtk.Align.END : Gtk.Align.START;
            case Gtk.Align.END:
                return this.get_direction() == Gtk.TextDirection.RTL ? Gtk.Align.START : Gtk.Align.END;
            default:
                return this.halign;
        }
    }
});

// Behaves like a weak map of JS objects, but without loosing
// the "reference" to the underlying GObject objects.
const GObjectMap = class extends Map {
    get(jsObject) {
        return super.get(System.addressOfGObject(jsObject));
    }

    set(jsObject, value) {
        return super.set(System.addressOfGObject(jsObject), value);
    }
};

var BlurOverlay = GObject.registerClass({
    GTypeName: 'DemoBlurOverlay',
    Properties: {
        'child': GObject.ParamSpec.object(
            'child', "Child", "The main child",
            GObject.ParamFlags.READWRITE, Gtk.Widget.$gtype
        ),
    },
}, class extends Gtk.Widget {
    get _childInfos() {
        return this.__childInfos || (this.__childInfos = new GObjectMap());
    }

    *[Symbol.iterator]() {
        for (let child = this.get_first_child(); child; child = child.get_next_sibling())
            yield child;
    }

    vfunc_measure(orientation, forSize) {
        let [min, nat, minBaseline, natBaseline] = [0, 0, -1, -1];

        [...this].forEach(child => {
            let [childMin, childNat, childMinBaseline, childNatBaseline] = child.measure(orientation, forSize);

            min = Math.max(min, childMin);
            nat = Math.max(nat, childNat);
            minBaseline = Math.max(minBaseline, childMinBaseline);
            natBaseline = Math.max(natBaseline, childNatBaseline);
        });

        return [min, nat, minBaseline, natBaseline];
    }

    vfunc_size_allocate(width, height, baseline) {
        [...this].forEach(child => {
            if (!child.visible)
                return;

            let allocation;

            if (child == this.child) {
                allocation = new Gdk.Rectangle({
                    x: 0, y: 0, width, height,
                });
            } else {
                let [childMinReq, childNatReq] = child.get_preferred_size();

                allocation = new Gdk.Rectangle({
                    x: 0, y: 0,
                    width: Math.max(childMinReq.width, Math.min(this.get_width(), childNatReq.width)),
                    height: Math.max(childMinReq.height, Math.min(this.get_height(), childNatReq.height)),
                });

                switch (this.effectiveHalign) {
                    case Gtk.Align.FILL:
                        allocation.width = Math.max(allocation.width, this.get_width());
                        break;
                    case Gtk.Align.CENTER:
                        allocation.x += this.get_width() / 2 - allocation.width / 2;
                        break;
                    case Gtk.Align.END:
                        allocation.x += this.get_width() - allocation.width;
                }

                switch (this.valign) {
                    case Gtk.Align.FILL:
                        allocation.height = Math.max(allocation.height, this.get_height());
                        break;
                    case Gtk.Align.CENTER:
                        allocation.y += this.get_height() / 2 - allocation.height / 2;
                        break;
                    case Gtk.Align.END:
                        allocation.y += this.get_height() - allocation.height;
                }

                let isLeft = child.effectiveHalign == Gtk.Align.START ? allocation.x == 0 : false;
                let isRight = child.effectiveHalign == Gtk.Align.END ? allocation.x + allocation.width == this.get_width() : false;
                let isTop = child.valign == Gtk.Align.START ? allocation.y == 0 : false;
                let isBottom = child.valign == Gtk.Align.END ? allocation.y + allocation.height == this.get_height() : false;

                child[isLeft ? 'add_css_class' : 'remove_css_class']('left');
                child[isRight ? 'add_css_class' : 'remove_css_class']('right');
                child[isTop ? 'add_css_class' : 'remove_css_class']('top');
                child[isBottom ? 'add_css_class' : 'remove_css_class']('bottom');
            }

            child.size_allocate(allocation, -1);
        });
    }

    vfunc_unrealize() {
        super.vfunc_unrealize();

        [...this].forEach(child => child.unparent());
    }

    vfunc_snapshot(snapshot) {
        let children = [...this];
        let mainChild = this.child;
        let overlayChildren = children.filter(child => child != mainChild);
        let blurChildren = overlayChildren.filter(child => this._childInfos.get(child).blur > 0);

        if (!mainChild || !blurChildren.length) {
            children.forEach(child => this.snapshot_child(child, snapshot));
            return;
        }

        // FIXME: There is a bug with "new Gtk.Snapshot()", not with "Gtk.Snapshot.new()".
        let mainChildSnapshot = Gtk.Snapshot.new();
        this.snapshot_child(mainChild, mainChildSnapshot);
        let mainChildNode = mainChildSnapshot.to_node();
        let appendMainChildNode = snapshot.append_node.bind(snapshot, mainChildNode);

        let clip = new Cairo.Region();
        clip.unionRectangle({ x: 0, y: 0, width: this.get_allocated_width(), height: this.get_allocated_height() });

        // Render the blur part of the main child.
        blurChildren.forEach(child => {
            snapshot.push_blur(this._childInfos.get(child).blur);
            snapshot.push_clip(child.get_allocation().toBounds());
            appendMainChildNode();
            snapshot.pop();
            snapshot.pop();

            clip.subtractRectangle(child.get_allocation());
        });

        // Render the non-blur part of the main child.
        for (let i = 0; i < clip.numRectangles(); i++) {
            snapshot.push_clip(clip.getRectangle(i).toBounds());
            appendMainChildNode();
            snapshot.pop();
        }

        // Finally render the other children.
        overlayChildren.forEach(child => {
            this.snapshot_child(child, snapshot);
        });
    }

    get child() {
        return [...this].find(child => this._childInfos.get(child).isMain) || null;
    }

    set child(widget) {
        let child = this.child;
        if (child == widget)
            return;

        if (child)
            child.unparent();
        if (widget) {
            widget.insert_after(this, null);
            this._childInfos.set(widget, { isMain: true });
        }
        this.notify('child');
    }

    addOverlay(widget, blur = 0) {
        widget.insert_before(this, null);
        this._childInfos.set(widget, { blur });
    }
});
