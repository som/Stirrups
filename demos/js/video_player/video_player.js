// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Video Player
 *
 * This is a simple video player using just GTK widgets.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, Gio, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');
const _ = imports.gettext.domain('gtk40').gettext;

let application = new Gtk.Application();

application.connect('activate', () => {
    let video = new Gtk.Video({ autoplay: true });

    let openButton = Gtk.Button.new_with_mnemonic(_("_Open"));
    openButton.connect('clicked', () => {
        let dialog = new Gtk.FileChooserNative({
            title: "Select a video",
            transientFor: openButton.get_root(), modal: true,
            action: Gtk.FileChooserAction.OPEN,
            acceptLabel: _("_Open"), cancelLabel: _("_Cancel"),
        });

        let filter = new Gtk.FileFilter({ name: "All Files" });
        filter.add_pattern('*');
        dialog.add_filter(filter);

        filter = new Gtk.FileFilter({ name: "Images" });
        filter.add_mime_type('image/*');
        dialog.add_filter(filter);

        filter = new Gtk.FileFilter({ name: "Video" });
        filter.add_mime_type('video/*');
        dialog.add_filter(filter);
        dialog.set_filter(filter);

        dialog.connect('response', (dialog_, response) => {
            if (response == Gtk.ResponseType.ACCEPT)
                video.file = dialog.get_file();

            dialog.unref();
            dialog.destroy();
        });

        dialog.show();

        // XXX: Prevent the dialog from being disposed after 7 seconds.
        dialog.ref();
    });

    let logoButton = new Gtk.Button({
        child: new Gtk.Image({
            file: directory.get_child('gtk_logo_cursor.png').get_path(),
            pixelSize: 24,
        }),
    });
    logoButton.connect('clicked', () => {
        video.file = directory.get_child('gtk-logo.webm');
    });

    let bbbButton = new Gtk.Button({
        child: new Gtk.Image({
            file: directory.get_child('bbb.png').get_path(),
            pixelSize: 24,
        }),
    });
    bbbButton.connect('clicked', () => {
        video.file = Gio.File.new_for_uri('https://download.blender.org/peach/trailer/trailer_400p.ogg');
    });

    let fullscreenButton = new Gtk.Button({ iconName: 'view-fullscreen-symbolic' });
    fullscreenButton.connect('clicked', () => fullscreenButton.root.fullscreen());

    let headerBar = new Gtk.HeaderBar();
    headerBar.pack_start(openButton);
    headerBar.pack_start(logoButton);
    headerBar.pack_start(bbbButton);
    headerBar.pack_end(fullscreenButton);

    let controller = new Gtk.ShortcutController({ scope: Gtk.ShortcutScope.GLOBAL });
    controller.add_shortcut(
        new Gtk.Shortcut({
            trigger: Gtk.KeyvalTrigger.new(Gdk.KEY_F11, 0),
            action: Gtk.CallbackAction.new(() => {
                let isFullscreen = controller.widget.root.get_surface().get_state() & Gdk.ToplevelState.FULLSCREEN;
                controller.widget.root[isFullscreen ? 'unfullscreen' : 'fullscreen']();
            }),
        })
    );

    let window = new Gtk.Window({
        application,
        title: "Video Player",
        defaultWidth: 600, defaultHeight: 400,
        child: video,
    });

    window.set_titlebar(headerBar);
    window.add_controller(controller);
    window.present();
});

application.run([]);
