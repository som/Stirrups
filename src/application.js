/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported Application */

const { Gio, GLib, GObject, Gtk, Pango } = imports.gi;
const { byteArray: ByteArray, system: System } = imports;
const { Window } = imports.window;

var Application = GObject.registerClass({
}, class Application extends Gtk.Application {
    _init() {
        super._init({ application_id: pkg.name, flags: Gio.ApplicationFlags.NON_UNIQUE });

        this.add_main_option('version', 'v'.charCodeAt(0), GLib.OptionFlags.NONE, GLib.OptionArg.NONE, "Show program version", null);

        const actions = [
            { name: 'about', callback: this._activateAbout.bind(this) },
            { name: 'quit', callback: this._activateQuit.bind(this) },
            { name: 'inspector', callback: this._activateInspector.bind(this) },
        ];

        const accels = [
            { actionAndTarget: 'app.about', accelerators: ['F1'] },
            { actionAndTarget: 'app.quit', accelerators: ['<Control>q'] },
        ];

        actions.forEach(action => {
            let simpleAction = new Gio.SimpleAction({ name: action.name });
            simpleAction.connect('activate', action.callback);
            this.add_action(simpleAction);
        });

        accels.forEach(accel => {
            this.set_accels_for_action(accel.actionAndTarget, accel.accelerators);
        });
    }

    vfunc_handle_local_options(options) {
        if (options.contains('version')) {
            print(pkg.version);
            return 0;
        }

        return -1;
    }

    vfunc_activate() {
        let window = new Window({ application: this });
        window.connect('close-request', window => {
            window.application = null;
            return false;
        });
        window.present();
    }

    _activateAbout() {
        let osName = GLib.get_os_info('NAME');
        let osVersion = GLib.get_os_info('VERSION_ID');
        let gjsVersion = System.version.toString();
        let pyGObjectVersion = "Unknown";

        try {
            let [success_, stdout] = GLib.spawn_command_line_sync('pkg-config --modversion pygobject-3.0');
            pyGObjectVersion = ByteArray.toString(stdout).trim() || pyGObjectVersion;
        } catch(e) {
            log(e.message);
        }

        let builder = Gtk.Builder.new_from_resource(`${pkg.resourceBasePath}/ui/about-dialog.ui`);
        let aboutDialog = builder.get_object('about_dialog');

        aboutDialog.transientFor = this.get_active_window();
        aboutDialog.systemInformation = (
            `OS\t${osName}${osVersion ? ` ${osVersion}` : ``}\n`
            + `\n`
            + `System libraries\n`
            + `\tGLib\t${GLib.MAJOR_VERSION}.${GLib.MINOR_VERSION}.${GLib.MICRO_VERSION}\n`
            + `\tPango\t${Pango.version_string()}\n`
            + `\tGTK\t${Gtk.MAJOR_VERSION}.${Gtk.MINOR_VERSION}.${Gtk.MICRO_VERSION}\n`
            + `\tGJS \t${gjsVersion.slice(0, 1)}.${gjsVersion.slice(1, 3)}.${gjsVersion.slice(3)}\n`
            + `\tPyGObject \t${pyGObjectVersion}`
        );
        aboutDialog.show();
    }

    _activateQuit() {
        this.get_windows().forEach(window => window.close());
    }

    _activateInspector() {
        Gtk.Window.set_interactive_debugging(true);
    }
});
