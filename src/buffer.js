/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { Gio, GObject, Gtk } = imports.gi;

let hasHighlightProgram = true;

// A buffer with syntax highlighting, using the 'highlight' program.
GObject.registerClass({
    GTypeName: 'StirrupsHighlightBuffer',
    Properties: {
        'dark-theme': GObject.ParamSpec.string(
            'dark-theme', "Dark theme", "The dark theme to use",
            GObject.ParamFlags.READWRITE, 'edit-vim-dark'
        ),
        'light-theme': GObject.ParamSpec.string(
            'light-theme', "Light theme", "The light theme to use",
            GObject.ParamFlags.READWRITE, 'edit-kwrite'
        ),
    },
}, class extends Gtk.TextBuffer {
    _init(params) {
        // TODO: Report issue to GJS.
        // Not to pass params because it contains 3 keys for the same 'tag-table' property.
        super._init();
    }

    insertWithHighlight(iter, text, dark = false, filename = '') {
        if (!hasHighlightProgram) {
            this.insert(iter, text, -1);

            return;
        }

        try {
            Gio.Subprocess.new(
                ['highlight', '--fragment', '--out-format=pango', `--style=${dark ? this.darkTheme : this.lightTheme}`, `--syntax-by-name=${filename}`],
                Gio.SubprocessFlags.STDIN_PIPE | Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE
            ).communicate_utf8_async(text, null, (subprocess, result) => {
                try {
                    let [success_, stdout, stderr] = subprocess.communicate_utf8_finish(result);

                    if (subprocess.get_exit_status() != 0 || !stdout) {
                        if (stderr)
                            log(stderr);

                        return;
                    }

                    // Remove the global font attributes.
                    let markup = stdout.slice(stdout.indexOf('>') + 1, stdout.lastIndexOf('<'));

                    this.insert_markup(iter, markup, -1);
                } catch(e) {
                    logError(e);

                    this.insert(iter, text, -1);
                }
            });
        } catch(e) {
            log("For syntax highlighting, install the “highlight” program");
            hasHighlightProgram = false;

            this.insert(iter, text, -1);
        }
    }
});
